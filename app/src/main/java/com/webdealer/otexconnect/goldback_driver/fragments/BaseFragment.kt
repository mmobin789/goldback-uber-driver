package com.webdealer.otexconnect.goldback_driver.fragments

import android.app.Dialog
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.widget.ImageView
import com.bumptech.glide.request.RequestOptions
import com.vansuita.pickimage.bundle.PickSetup
import com.vansuita.pickimage.dialog.PickImageDialog
import com.vansuita.pickimage.listeners.IPickResult
import com.webdealer.otexconnect.goldback_driver.activities.BaseUI
import com.webdealer.otexconnect.goldback_driver.activities.JobsActivity
import com.webdealer.otexconnect.goldback_driver.interfaces.OnFragmentBackPressListener

abstract class BaseFragment : Fragment(), OnFragmentBackPressListener {
    lateinit var progressBar: Dialog
    lateinit var jobUI: JobsActivity
    var uid = -1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        progressBar = BaseUI.createProgressBar(context!!)
        jobUI = activity as JobsActivity
        jobUI.setOnFragmentBackPressListener(this)
        uid = getPrefs().getInt("mId", -1)
    }

    fun fleetMembership(): Boolean {

        return getPrefs().getInt("fleet", -1) != -1

    }

    fun pickImage(iPickResult: IPickResult) {
        PickImageDialog.build(PickSetup().setSystemDialog(true), iPickResult).show(childFragmentManager).apply {
            setOnPickCancel { dismiss() }
        }
    }

    fun getPrefs(): SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    fun loadWithGlide(path: String, iv: ImageView, isCircle: Boolean) {
        val request = GlideApp.with(this).load(path).placeholder(R.drawable.logo)
        if (isCircle)
            request.apply(RequestOptions.circleCropTransform())
        request.into(iv)
    }

//    protected fun moveToFragment(childFragmentManager: FragmentManager, baseFragment: BaseFragment, state: String) {
//
//        childFragmentManager.beginTransaction().replace(R.id.content_frame, baseFragment).addToBackStack(state).commit()
//    }


}