package com.webdealer.otexconnect.goldback_driver.network;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.webdealer.otexconnect.goldback_driver.interfaces.OnBidListener;
import com.webdealer.otexconnect.goldback_driver.interfaces.OnCompletedJobsListener;
import com.webdealer.otexconnect.goldback_driver.interfaces.OnEarningsListener;
import com.webdealer.otexconnect.goldback_driver.interfaces.OnFCMUpdateListener;
import com.webdealer.otexconnect.goldback_driver.interfaces.OnMessageListener;
import com.webdealer.otexconnect.goldback_driver.interfaces.OnMyWayListener;
import com.webdealer.otexconnect.goldback_driver.interfaces.OnQRCodePostedListener;
import com.webdealer.otexconnect.goldback_driver.interfaces.OnQrCodesListener;
import com.webdealer.otexconnect.goldback_driver.interfaces.OnWithdrawListener;
import com.webdealer.otexconnect.goldback_driver.models.APIResponse;
import com.webdealer.otexconnect.goldback_driver.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Fazal on 3/13/18.
 */

public class RestClient {

    private static Retrofit retrofit;
    private static Gson gson;

    private static Retrofit getClient() {
        if (retrofit == null) {
//        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
//        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//
//        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
//        httpClient.addInterceptor(logging);


            String BASE_URL = "http://goldback.business/api/";
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    // .client(httpClient.build())
                    .build();
            gson = new Gson();
        }
        return retrofit;
    }

    public static WebService getServices() {

        return getClient().create(WebService.class);
    }

    public static void withDrawBid(int bidID, OnBidListener onBidListener) {
        getServices().deleteBid(bidID).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        Log.i("WithdrawBidAPI", jsonObject + "");
                        if (jsonObject.getBoolean("status"))
                            onBidListener.onBidUpdated();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.e("WithdrawBidAPI", t.toString());
            }
        });
    }

    public static void changeBid(int bidID, double amount, OnBidListener onBidListener) {
        getServices().updateBid(bidID, amount).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        Log.i("ChangeBidAPI", response.body().string());
                        onBidListener.onBidUpdated();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.e("ChangeBidAPI", t.toString());
            }
        });
    }

    public static void withDraw(int id, OnWithdrawListener onWithdrawListener) {
        getServices().withdraw(id).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {

                if (response.isSuccessful() && response.body().getStatus()) {
                    Log.i("WithdrawAPI", gson.toJson(response.body()));
                    onWithdrawListener.onWithdrawSuccess(response.body().message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e("WithdrawAPI", t.toString());
            }
        });
    }

    public static void sendMessageToFleet(int driverID, String message, OnMessageListener listener) {
        getServices().sendMessageToFleet(driverID, message).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {

                if (response.isSuccessful()) {
                    Log.i("sendMessageFleetAPI", gson.toJson(response.body()));
                    listener.onMessageSent();
                }
            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e("sendMessageFleetAPI", t.toString());
            }
        });
    }

    public static void sendMessage(int orderID, String message, OnMessageListener listener) {
        getServices().sendMessageToCustomer(orderID, message).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {

                if (response.isSuccessful()) {
                    Log.i("sendMessageAPI", gson.toJson(response.body()));
                    listener.onMessageSent();
                }
            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e("sendMessageAPI", t.toString());
            }
        });

    }

    public static void sendDriverLocation(String orderID, LatLng latLng) {
        getServices().sendLocation(orderID, latLng.latitude + "", latLng.longitude + "").enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        Log.i("sendLocationAPI", response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.e("sendLocationAPI", t.toString());
            }
        });
    }

    public static void getQrCodes(String orderID, OnQrCodesListener listener) {
        getServices().getQrCodes(orderID).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {
                if (response.isSuccessful()) {
                    Log.i("QrCodesAPI", gson.toJson(response.body()));
                    listener.onQrCodes(response.body().getData().qrCodes);
                }
            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e("QrCodesAPI", t.toString());
            }
        });
    }

    public static void onMyWay(String orderID, OnMyWayListener onMyWayListener) {
        getServices().onMyWay(orderID).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String json = response.body().string();
                        JSONObject jsonObject = new JSONObject(json);
                        if (jsonObject.getBoolean("status")) {
                            onMyWayListener.onMyWay();
                        }
                        Log.i("OnMyWayAPI", jsonObject.toString());
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.e("OnMyWayAPI", t.toString());
            }
        });
    }

    public static void getEarnings(int id, OnEarningsListener onEarningsListener) {
        getServices().getDriverEarnings(id).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {
                if (response.isSuccessful()) {
                    Log.i("EarningsAPI", gson.toJson(response.body()));
                    onEarningsListener.onEarnings(response.body().getData().drivers);
                }
            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e("EarningsAPI", t.toString());
            }
        });
    }

    public static void contactUs(int id, String message) {
        getServices().contactUs(id, message).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {
                if (response.isSuccessful()) {
                    Log.i("ContactUSAPI", gson.toJson(response.body()));


                }
            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e("ContactUSApi", t.toString());
            }
        });
    }

    public static void getDoneJobs(int id, OnCompletedJobsListener listener) {
        getServices().getDoneJobs(id).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {
                if (response.isSuccessful()) {
                    Log.i("CompletedJobsAPI", gson.toJson(response.body()));
                    listener.onJobsCompleted(response.body().getData().getOrders());
                } else listener.onFailed();

            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e("CompletedJobsAPI", t.toString());
                listener.onFailed();
            }
        });
    }

    public static void rejectJob(String orderID, OnMessageListener onMessageListener) {
        getServices().rejectJob(orderID).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {
                if (response.isSuccessful()) {
                    Log.i("RejectJobAPI", gson.toJson(response.body()));
                    onMessageListener.onMessageSent();
                }
            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e("RejectJobAPI", t.toString());
            }
        });
    }

    public static void acceptJob(int id, String orderID, OnMyWayListener onMyWayListener) {
        getServices().acceptJob(id, orderID).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {
                if (response.isSuccessful()) {
                    Log.i("AcceptJobAPI", gson.toJson(response.body()));
                    onMyWayListener.onMyWay();
                }
            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e("AcceptJobAPI", t.toString());
            }
        });
    }

    public static MultipartBody.Part getImageRequestBody(Context context, File file, String key) {
        File compressed = Utils.getCompressedFile(context, file);
        assert compressed != null;
        Log.d("compressedImage", compressed.getPath());
        RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), compressed);
        return MultipartBody.Part.createFormData(key, compressed.getName(), mFile);
    }

    private static RequestBody getTextRequestBody(String data) {
        return RequestBody.create(MediaType.parse("text/plain"), data);
    }

    public static void postInsuranceImage(Context context, File image, int id, OnMessageListener onMessageListener) {
        getServices().postInsuranceImage(getImageRequestBody(context, image, "insuranceURL"), id).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {
                if (response.isSuccessful() && response.body().getStatus()) {
                    onMessageListener.onMessageSent();
                }
            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e("PostInsuranceImageAPI", t.toString());
            }
        });
    }

    public static void uploadDriverImage(Dialog dialog, String driverID, File file) {
        getServices().uploadDriverImage(getImageRequestBody(dialog.getContext(), file, "profile_image"), getTextRequestBody(driverID)).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {
                Log.i("DriverImageAPI", gson.toJson(response.body()));
                if (response.body().getStatus()) {
                    Toast.makeText(dialog.getContext(), "Upload Success", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(dialog.getContext(), "Something went Wrong", Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e("DriverImageAPI", t.toString());
                dialog.dismiss();
            }
        });
    }

    public static void postQR(String qrCode, String orderID, File compressed, OnQRCodePostedListener onQRCodePostedListener) {

        Log.d("compressedImage", compressed.getPath());
        RequestBody mFile = RequestBody.create(MediaType.parse("image/png"), compressed);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("imageURL", compressed.getName(), mFile);
        getServices().postQR(fileToUpload, getTextRequestBody(qrCode), getTextRequestBody(orderID)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        Log.i("PostQRAPI", response.body().string());
                        onQRCodePostedListener.onQRCodeSent();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.e("PostQRAPI", t.toString());
            }
        });
    }

    public static void updateFCM(int uid, String token, OnFCMUpdateListener listener) {
        getServices().updateFCM(uid + "", token).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {
                if (response.body().getStatus()) {
                    Log.i("UpdateFCMAPI", "Token Updated");
                    listener.onTokenUpdated();
                }
            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e("UpdateFCMAPI", t.toString());
            }
        });
    }


}
