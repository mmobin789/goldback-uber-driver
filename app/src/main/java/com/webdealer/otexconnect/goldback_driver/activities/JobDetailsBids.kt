package com.webdealer.otexconnect.goldback_driver.activities

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.Toast
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.webdealer.otexconnect.goldback_driver.interfaces.OnBidListener
import com.webdealer.otexconnect.goldback_driver.models.Bid
import com.webdealer.otexconnect.goldback_driver.network.RestClient
import kotlinx.android.synthetic.main.activity_job_details_bidded_on.*
import kotlinx.android.synthetic.main.change_bid_dialog.*

class JobDetailsBids : BaseUI(), OnMapReadyCallback, OnBidListener {

    var p = ""
    var d = ""
    lateinit var from: LatLng
    lateinit var to: LatLng
    var bid: Bid? = null
    override fun onMapReady(p0: GoogleMap) {
        setMapOptions(p0)
        from = encodeAddress(p)
        to = encodeAddress(d)
        drawPolyLines(p0, from, to)
    }

    override fun onBackPressed() {
        finish()
    }

    private fun getData(key: String): String = intent.getStringExtra(key)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_job_details_bidded_on)
        val mapUI = map as SupportMapFragment
        mapUI.getMapAsync(this)
        p = getData("pickup_location")
        d = getData("dropoff_location")
        val des = getData("description")
        val price = getData("budget")
        pickUpTV.text = p
        dropOffTV.text = d
        budget.text = price
        desc.text = des
        bid = intent.getParcelableExtra("bid")

        withDrawBtn.setOnClickListener {
            if (bid != null)
                RestClient.withDrawBid(bid!!.id, this)
            else Toast.makeText(it.context, "Not Bidded", Toast.LENGTH_SHORT).show()
        }
        changeBidBtn.setOnClickListener {
            if (bid != null)
                changeBidUI(it)
            else Toast.makeText(it.context, "Not Bidded", Toast.LENGTH_SHORT).show()
        }


    }

    override fun onBidUpdated() {
        Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show()
    }

    private fun changeBidUI(v: View) {
        val dialog = Dialog(v.context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.change_bid_dialog)
        dialog.submit_btn.setOnClickListener {
            if (dialog.etBid.length() > 1) {
                RestClient.changeBid(bid!!.id, dialog.etBid.text.toString().toDouble(), this)
            }
        }

        dialog.show()
    }
}
