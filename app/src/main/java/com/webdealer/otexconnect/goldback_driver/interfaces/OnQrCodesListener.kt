package com.webdealer.otexconnect.goldback_driver.interfaces

interface OnQrCodesListener {
    fun onQrCodes(qrCodes: List<String>)
}