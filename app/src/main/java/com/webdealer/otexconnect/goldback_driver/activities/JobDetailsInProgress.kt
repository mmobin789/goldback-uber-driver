package com.webdealer.otexconnect.goldback_driver.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.maps.android.SphericalUtil
import com.google.zxing.integration.android.IntentIntegrator
import com.webdealer.otexconnect.goldback_driver.interfaces.OnMyWayListener
import com.webdealer.otexconnect.goldback_driver.interfaces.OnQrCodesListener
import com.webdealer.otexconnect.goldback_driver.network.RestClient
import com.webdealer.otexconnect.goldback_driver.services.LocationSync
import com.webdealer.otexconnect.goldback_driver.utils.Utils
import kotlinx.android.synthetic.main.activity_job_detail_inprogress.*
import kotlinx.android.synthetic.main.job_detail_inprogress.*

class JobDetailsInProgress : BaseUI(), OnMapReadyCallback {
    var p = ""
    var d = ""
    var orderID = ""
    lateinit var qrCodes: List<String>
    private fun getData(key: String): String = intent.getStringExtra(key)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_job_detail_inprogress)
        val mapUI = map as SupportMapFragment
        mapUI.getMapAsync(this)
        p = getData("pickup_location")
        d = getData("dropoff_location")
        pickUpTV.text = p
        dropOffTV.text = d
        orderID = getData("id")
        Log.i("orderID", orderID)

        contact.setOnClickListener {
            val chat = Intent(it.context, ChatActivity::class.java)
            chat.putExtra("order_id", orderID)
            startActivity(chat)
        }
        onWay.setOnClickListener {
            RestClient.onMyWay(orderID, object : OnMyWayListener {
                override fun onMyWay() {
                    onWay.text = "Package Picked"
                    locationSync()
                    scanQR()
                }
            })

        }

        getQrCodes()
    }

    private fun locationSync() {
        val locationSync = Intent(this, LocationSync::class.java)
        locationSync.putExtra("orderID", orderID)
        startService(locationSync)
    }


    private fun scanQR() {
        IntentIntegrator(this).setCaptureActivity(ScanQR::class.java).initiateScan()

    }

    private fun getQrCodes() {
        progressBar.show()
        RestClient.getQrCodes(orderID, object : OnQrCodesListener {
            override fun onQrCodes(qrCodes: List<String>) {
                progressBar.dismiss()
                this@JobDetailsInProgress.qrCodes = qrCodes

            }
        })
    }

    private fun uploadPackageUI(qrCode: String) {
        val uploadUI = Intent(this, UploadPackage::class.java)
        uploadUI.putExtra("qrCode", qrCode)
        uploadUI.putExtra("orderID", orderID)
        startActivityForResult(uploadUI, 3)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 3) {
            onWay.text = "Package Delivered"
        } else {
            val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
            if (result != null) {
                if (!result.contents.isNullOrBlank()) {
                    val qrCode = getValidatedQrCode(result.contents)
                    if (qrCode.isNotBlank()) {
                        uploadPackageUI(qrCode)
                    } else {
                        Toast.makeText(this, "Invalid QR Code", Toast.LENGTH_SHORT).show()
                        scanQR()
                    }
                }
            }
        }

    }

    private fun getValidatedQrCode(qrCode: String): String {
        var s = ""
        for (qr in qrCodes) {
            if (qr == qrCode) {
                s = qr
                Toast.makeText(this, "$s (Qr Code Validated)", Toast.LENGTH_SHORT).show()
                break
            }
        }
        return s
    }


    override fun onMapReady(p0: GoogleMap) {
        setMapOptions(p0)
        val from = encodeAddress(p)
        val to = encodeAddress(d)
        drawPolyLines(p0, from, to)
        distance.text = Utils.meterToMiles(SphericalUtil.computeDistanceBetween(from, to))
    }
}
