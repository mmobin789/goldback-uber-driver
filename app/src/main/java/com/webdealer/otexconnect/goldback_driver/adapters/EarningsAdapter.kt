package com.webdealer.otexconnect.goldback_driver.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.webdealer.otexconnect.goldback_driver.models.Earning
import kotlinx.android.synthetic.main.earnings_row.*

class EarningsAdapter(private val earnings: List<Earning>) : RecyclerView.Adapter<ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.earnings_row, parent, false))
    }

    override fun getItemCount(): Int {
        return earnings.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val earning = earnings[position]
        holder.desc.text = earning.description
        if (earning.earning != null)
            holder.earningTV.text = earning.earning
    }
}