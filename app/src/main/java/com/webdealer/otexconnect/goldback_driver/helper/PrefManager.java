package com.webdealer.otexconnect.goldback_driver.helper;

/**
 * Created by fazal on 26/03/2018.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.webdealer.otexconnect.goldback_driver.models.ChatMessage;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


public class PrefManager {
    private static final String KEY_IS_LOGGED_IN = "isLoggedIn";
    private static final Integer KEY_ID = 0;
    private static final String KEY_NAME = "name";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_MOBILE = "mobile";
    // Shared Preferences
    public SharedPreferences pref;

    public PrefManager(Context context) {
        pref = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public List<ChatMessage> loadChat(String id) {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<ChatMessage>>() {
        }.getType();
        String json = pref.getString(id, "");
        List<ChatMessage> list = gson.fromJson(json, listType);
        if (list == null)
            list = new ArrayList<>();
        return list;
    }

    public void saveChat(String id, List<ChatMessage> chatMessageList) {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<ChatMessage>>() {
        }.getType();
        String json = gson.toJson(chatMessageList, listType);
        pref.edit().putString(id, json).apply();

    }

    public void removeString(String key) {
        pref.edit().remove(key).apply();
    }

    public void saveString(String key, String s) {
        pref.edit().putString(key, s).apply();
    }

    public String getString(String key) {
        return pref.getString(key, "N/A");
    }


    public void createLogin(Integer mId, String name, String email, String mobile) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(String.valueOf(KEY_ID), mId);
        editor.putString(KEY_NAME, name);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_MOBILE, mobile);
        editor.putBoolean(KEY_IS_LOGGED_IN, true);
        editor.apply();
    }


    public boolean isLoggedIn() {
        return pref.getBoolean(KEY_IS_LOGGED_IN, false);
    }

    public void clearSession() {
        pref.edit().clear().apply();
    }


}
