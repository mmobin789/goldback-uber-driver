package com.webdealer.otexconnect.goldback_driver.activities

import android.app.Activity
import android.os.Bundle
import android.widget.Toast
import com.vansuita.pickimage.bean.PickResult
import com.vansuita.pickimage.listeners.IPickResult
import com.webdealer.otexconnect.goldback_driver.interfaces.OnQRCodePostedListener
import com.webdealer.otexconnect.goldback_driver.network.RestClient
import kotlinx.android.synthetic.main.activity_upload_package.*
import java.io.File

class UploadPackage : BaseUI(), IPickResult, OnQRCodePostedListener {


    var qrCode = ""
    var orderID = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload_package)
        qrCode = intent.getStringExtra("qrCode")
        orderID = intent.getStringExtra("orderID")
        add.setOnClickListener {
            pickImage(this, this)
        }
    }

    override fun onPickResult(p0: PickResult) {
        progressBar.show()
        loadWithGlide(p0.path, qrPhoto, true)
        RestClient.postQR(qrCode, orderID, File(p0.path), this)
    }

    override fun onQRCodeSent() {
        Toast.makeText(this, "QR Code Sent", Toast.LENGTH_SHORT).show()
        progressBar.dismiss()
        setResult(Activity.RESULT_OK)
        finish()
    }
}
