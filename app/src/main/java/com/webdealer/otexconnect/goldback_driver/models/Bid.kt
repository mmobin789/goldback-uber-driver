package com.webdealer.otexconnect.goldback_driver.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Bid(val id: Int, val bid: String) : Parcelable