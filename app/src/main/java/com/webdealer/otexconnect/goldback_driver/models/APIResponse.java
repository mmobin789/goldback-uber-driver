package com.webdealer.otexconnect.goldback_driver.models;

/**
 * Created by fazal on 26/03/2018.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class APIResponse {

    @SerializedName("msg")
    public String message;
    private boolean status;
    private Data data;

    @SerializedName("error")
    @Expose
    private APIError error;

    public boolean getStatus() {
        return status;
    }

    public APIError getError() {
        return error;
    }

    public Data getData() {
        return data;
    }
}
