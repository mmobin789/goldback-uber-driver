package com.webdealer.otexconnect.goldback_driver.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.webdealer.otexconnect.goldback_driver.adapters.EarningsAdapter
import com.webdealer.otexconnect.goldback_driver.interfaces.OnEarningsListener
import com.webdealer.otexconnect.goldback_driver.interfaces.OnWithdrawListener
import com.webdealer.otexconnect.goldback_driver.models.Driver
import com.webdealer.otexconnect.goldback_driver.network.RestClient
import kotlinx.android.synthetic.main.fragment_earnings.*
import kotlinx.android.synthetic.main.fragment_withdraw.*
import kotlinx.android.synthetic.main.fragment_withdraw_detail.*

class EarningsFragment : BaseFragment(), OnWithdrawListener {
    private var page = 1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_earnings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rv.layoutManager = LinearLayoutManager(context)

        withdrawBtn.setOnClickListener {
            withDrawUI.visibility = View.VISIBLE
            page = 2
            jobUI.header.text = "WithDraw"
        }

        rl.setOnClickListener {
            withDrawUI.visibility = View.GONE
            withDrawDetailUI.visibility = View.VISIBLE
            page = 3
        }

        withdraw.setOnClickListener {
            RestClient.withDraw(uid, this)
        }

        progressBar.show()
        RestClient.getEarnings(uid, object : OnEarningsListener {
            override fun onEarnings(drivers: List<Driver>) {
                earningsW.text = drivers[0].earnedAmount
                Log.i("earnings", drivers[0].earnings.size.toString())
                rv.adapter = EarningsAdapter(drivers[0].earnings)
                progressBar.dismiss()
            }
        })
    }

    override fun onWithdrawSuccess(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun onFragmentBackPressed() {

        if (page == 3) {
            withDrawDetailUI.visibility = View.GONE
            withDrawUI.visibility = View.VISIBLE
            page--
        } else if (page == 2) {
            withDrawUI.visibility = View.GONE
            jobUI.header.text = "Earnings"


        }

    }

    companion object {
        @JvmStatic
        fun newInstance(): EarningsFragment = EarningsFragment()
    }
}