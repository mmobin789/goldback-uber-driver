package com.webdealer.otexconnect.goldback_driver.adapters

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.EditText
import com.webdealer.otexconnect.goldback_driver.activities.BaseUI
import com.webdealer.otexconnect.goldback_driver.interfaces.OnMessageListener
import com.webdealer.otexconnect.goldback_driver.models.ChatMessage
import com.webdealer.otexconnect.goldback_driver.network.RestClient

class FleetChatAdapter(private val baseUI: BaseUI) : RecyclerView.Adapter<ViewHolder>() {
    private val driver = 0
    private val fleet = 1
    private val userID = baseUI.getPrefs().getInt("mId", -1)
    private val list: MutableList<ChatMessage> = baseUI.prefManager.loadChat(userID.toString())

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layout = if (viewType == driver) {
            R.layout.sender
        } else {
            R.layout.receiver
        }
        return ViewHolder(LayoutInflater.from(parent.context).inflate(layout, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(baseUI, list[position], userID)
    }

    override fun getItemViewType(position: Int): Int {
        val chatMessage = list[position]
        return if (chatMessage.uid == userID)
            driver
        else fleet
    }

    fun receiveMessage(message: String, senderID: Int) {
        val chatMessage = ChatMessage(senderID, message, "N/A")
        list.add(chatMessage)
        notifyItemInserted(list.size)
        baseUI.prefManager.saveChat(userID.toString(), list)
    }

    fun sendMessage(inputField: EditText, message: String) {
        baseUI.progressBar.show()
        val chatMessage = ChatMessage(userID, message, baseUI.avatar)
        RestClient.sendMessageToFleet(userID, message, object : OnMessageListener {
            override fun onMessageSent() {
                baseUI.progressBar.dismiss()
                Log.i("Chat", "MessageSent")
                list.add(chatMessage)
                notifyItemInserted(list.size)
                inputField.setText("")
                baseUI.prefManager.saveChat(userID.toString(), list)


            }
        })

    }

    override fun getItemCount(): Int {
        return list.size
    }
}