package com.webdealer.otexconnect.goldback_driver.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.webdealer.otexconnect.goldback_driver.network.RestClient
import kotlinx.android.synthetic.main.fragment_contact_us.*

class ContactUsFragment : BaseFragment() {
    override fun onFragmentBackPressed() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_contact_us, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        send.setOnClickListener {
            if (etMsg.length() > 0)
                RestClient.contactUs(getPrefs().getInt("mId", -1), etMsg.text.toString())
            else Toast.makeText(it.context, "Enter a Message", Toast.LENGTH_SHORT).show()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = ContactUsFragment()
    }
}