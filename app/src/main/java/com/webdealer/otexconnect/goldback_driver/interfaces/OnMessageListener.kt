package com.webdealer.otexconnect.goldback_driver.interfaces

interface OnMessageListener {
    fun onMessageSent()
}