package com.webdealer.otexconnect.goldback_driver.services

import android.Manifest
import android.app.Service
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.IBinder
import android.support.v4.app.ActivityCompat
import android.util.Log
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng
import com.webdealer.otexconnect.goldback_driver.activities.BaseUI
import com.webdealer.otexconnect.goldback_driver.network.RestClient


class LocationSync : Service(), GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {


    private val locationDelay: Long = 20 // seconds
    private var orderID = ""
    // private var socket: Socket? = null

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }


    override fun onConnectionFailed(p0: ConnectionResult) {

    }

//    private fun initSocket(): Socket? {
//        try {
//            val socket = IO.socket("http://goldback.business/api/latlong")
//            socket.connect()
//            return socket
//
//        } catch (e: URISyntaxException) {
//            e.printStackTrace()
//        }
//        return null
//
//    }

    override fun onConnected(p0: Bundle?) {
        // socket = initSocket()
        startLocationUpdate()
    }

    private fun startLocationUpdate() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {


            LocationServices.getFusedLocationProviderClient(this).requestLocationUpdates(BaseUI.getLocationRequest(locationDelay), locationCallback, null)
        }
    }

    private fun stopLocationUpdate() {
        LocationServices.getFusedLocationProviderClient(this).removeLocationUpdates(locationCallback)

    }


    override fun onConnectionSuspended(p0: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null)
            orderID = intent.getStringExtra("orderID")
        BaseUI.buildGoogleApiClient(this, this, this).connect()

        return START_REDELIVER_INTENT
    }


    private fun sendLocation(location: LocationResult) {

        val lat = location.lastLocation.latitude
        val lng = location.lastLocation.longitude
        Log.v("LocationSync", lat.toString() + "/" + lng)
        val latLng = LatLng(lat, lng)
        Log.i("locationDelay", "After $locationDelay seconds")
        //  Toast.makeText(this, latLng.latitude.toString() + "/" + latLng.longitude, Toast.LENGTH_SHORT).show()
//        val jsonObject = JSONObject()
//        jsonObject.put("order_id", orderID)
//        jsonObject.put("lat", latLng.latitude)
//        jsonObject.put("long", latLng.longitude)
//        val data = jsonObject.toString()
        RestClient.sendDriverLocation(orderID, latLng)

//        socket!!.emit(data).on(data, {
//            val obj = it[0] as JSONObject
//            Log.i("DriverLocationAPI", obj.toString())
//
//        })

    }


    //   private var location: LocationResult? = null
    private val locationCallback = object : LocationCallback() {

        override fun onLocationResult(locationResult: LocationResult?) {

            sendLocation(locationResult!!)
        }
    }

    override fun onDestroy() {
        stopLocationUpdate()
        // socket!!.disconnect()
    }

//    inner class LocationBinder : Binder() {
//        //   fun getService(): LocationSync = this@LocationSync
//    }

}
