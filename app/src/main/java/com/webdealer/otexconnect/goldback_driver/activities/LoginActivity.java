package com.webdealer.otexconnect.goldback_driver.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.webdealer.otexconnect.goldback_driver.R;
import com.webdealer.otexconnect.goldback_driver.models.Login;
import com.webdealer.otexconnect.goldback_driver.models.User_;
import com.webdealer.otexconnect.goldback_driver.network.RestClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseUI implements View.OnClickListener {

    EditText mPasswordEditText;
    EditText mEmailEditText;
    LinearLayout mLoginButton;
    TextView mLoginText;
    Typeface mFont;
    Typeface mFontBold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
    }

    private void initView() {
        mPasswordEditText = findViewById(R.id.password);
        mEmailEditText = findViewById(R.id.email);
        mLoginButton = findViewById(R.id.login);
        mLoginText = findViewById(R.id.logintext);


        mLoginButton.setOnClickListener(this);
        mFont = Typeface.createFromAsset(getAssets(), "fonts/Neris-Light.otf");
        mFontBold = Typeface.createFromAsset(getAssets(), "fonts/Neris-SemiBold.otf");
        mLoginText.setTypeface(mFontBold);
        mPasswordEditText.setTypeface(mFont);
        mEmailEditText.setTypeface(mFont);

        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
        prefEditor.clear();
        prefEditor.apply();


        //getSupportActionBar().hide();
    }

    private void storePrefs(int mId, int fleetID, String mName, String mEmail, String mPhone, String payment_id, String profileImage, String licenseImage, String vehicleImage, String insuranceImage) {
        SharedPreferences.Editor prefEditor = prefManager.pref.edit();
        prefManager.clearSession();
        prefEditor.putInt("mId", mId);
        prefEditor.putInt("fleet", fleetID);
        prefEditor.putString("mName", mName);
        prefEditor.putString("mEmail", mEmail);
        prefEditor.putString("mPhone", mPhone);
        prefEditor.putString("payment_id", payment_id);
        //    prefEditor.putString("ban_status", ban_status);
//        prefEditor.putString("email_verification_status", email_verification_status);
//        prefEditor.putString("phone_number_verification_status", phone_number_verification_status);
//        prefEditor.putString("license_verification_status", phone_number_verification_status);
//        prefEditor.putString("vehicle_verification_status", vehicle_verification_status);
        prefEditor.putString("img", profileImage);
        prefEditor.putString("license", licenseImage);
        prefEditor.putString("vehicle", vehicleImage);
        prefEditor.putString("insurance", insuranceImage);
        prefEditor.apply();

        prefManager.createLogin(mId, mName, mEmail, mPhone);
    }

//    private void getPrefs() {
//        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//        int mId = prefs.getInt("mId", 0);
//        String mName = prefs.getString("mName", "DefaultName");
//        String mEmail = prefs.getString("mEmail", "email");
//        String mPhone = prefs.getString("mPhone", "phone");
//        String mfck = prefs.getString("mfck", "fck");
//
//    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.login) {
            if (validate()) {
                progressBar.show();
                login(mEmailEditText.getText().toString(), mPasswordEditText.getText().toString());
            } else {
                Toast.makeText(this, "Please Enter email and password!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean validate() {

        return !mPasswordEditText.getText().toString().equals("") && !mPasswordEditText.getText().toString().isEmpty() && !mEmailEditText.getText().toString().equals("") && !mEmailEditText.getText().toString().isEmpty();
    }

    private void login(String email, String password) {
        String e = email;
        String p = password;

        try {

            RestClient.getServices().login(email, password).enqueue(new Callback<Login>() {
                @Override
                public void onResponse(Call<Login> call, Response<Login> response) {

                    if (response.isSuccessful()) {

                        //-------------------------------
                        boolean s = response.body().getStatus();
                        if (s) {

                            response.body(); // have your all data
                            User_ user = response.body().getData().getUser();

                            storePrefs(user.getId(), user.fleetId, user.getName(), user.getEmail(), user.getPhoneNumber(), user.getPayment_id(), user.profileImage, user.licenseURL, user.vehicleURL, user.insuranceURL);

                            if (user.getBanStatus().equals("1")) {
                                Toast.makeText(LoginActivity.this, "Your account is banned by admin", Toast.LENGTH_SHORT).show();


                                return;

                            }


                            if (user.getPhoneNumberVerificationStatus().equals("0")) {
                                Toast.makeText(LoginActivity.this, "Phone Verification Required ", Toast.LENGTH_SHORT).show();

                                Intent intent = new Intent(getApplicationContext(), VerificationActivity.class);
                                startActivity(intent);
                                return;

                            }

                            progressBar.dismiss();

                            String mFCM = FirebaseInstanceId.getInstance().getToken();
                            RestClient.updateFCM(user.getId(), mFCM, () -> {
                                Toast.makeText(LoginActivity.this, "FCM Updated ", Toast.LENGTH_SHORT).show();
                            });


                            if (user.getVehicle_verification_status().equals("0")) {
                                Intent intent = new Intent(LoginActivity.this, Vehicle_registration.class);
                                startActivity(intent);
                            } else {

                                Intent intent = new Intent(LoginActivity.this, JobsActivity.class);
                                startActivity(intent);
                                closeKeyboard();
                                //Toast.makeText(LoginActivity.this, "Login ", Toast.LENGTH_SHORT).show();
                            }


                        } else {

//                            APIResponse error = ErrorUtils.parseError(response);
//                            Toast.makeText(getApplicationContext(), error.getStatus().toString(), Toast.LENGTH_SHORT).show();

                            progressBar.dismiss();
                            Toast.makeText(getApplicationContext(), "Error: " + response.body().getError().getDescription(), Toast.LENGTH_SHORT).show();


                        }

                        //------------------------------

                    } else {

                        Toast.makeText(getApplicationContext(), "Server Response Error ", Toast.LENGTH_SHORT).show();
                    }


                }

                @Override
                public void onFailure(Call<Login> call, Throwable t) {
                    progressBar.dismiss();
                    Toast.makeText(LoginActivity.this, "No Internet Connection " + t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        } catch (Exception ex) {
            Log.e("Login Ex", ex.getMessage());
        }
    }


    public void closeKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mPasswordEditText.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(mEmailEditText.getWindowToken(), 0);
    }
}
