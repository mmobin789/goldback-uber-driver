package com.webdealer.otexconnect.goldback_driver.fragments

import android.os.Bundle
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems
import kotlinx.android.synthetic.main.fragment_myjobs.*

class MyJobsFragment : BaseFragment() {
    override fun onFragmentBackPressed() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {


        return inflater.inflate(R.layout.fragment_myjobs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val pages = FragmentPagerItems.with(context).apply {
            add("Scheduled Jobs", JobTypeFragment::class.java)
            add("Done Jobs", JobTypeFragment::class.java)


        }
        val adapter = FragmentPagerItemAdapter(childFragmentManager, pages.create())
        viewpager.adapter = adapter
        viewpagertab.setViewPager(viewpager)
        viewpagertab.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageSelected(position: Int) {
                val jobTypeFragment = adapter.getPage(position) as JobTypeFragment
                if (position == 0) {
                    jobTypeFragment.showScheduled()
                } else {
                    jobTypeFragment.showCompleted()
                }
            }

            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }
        })

    }


    companion object {
        @JvmStatic
        fun newInstance(): MyJobsFragment = MyJobsFragment()
    }

}