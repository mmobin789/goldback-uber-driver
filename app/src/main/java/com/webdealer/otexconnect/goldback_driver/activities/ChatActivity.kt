package com.webdealer.otexconnect.goldback_driver.activities

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.google.gson.Gson
import com.webdealer.otexconnect.goldback_driver.adapters.ChatAdapter
import com.webdealer.otexconnect.goldback_driver.models.Orders
import kotlinx.android.synthetic.main.activity_chat.*

class ChatActivity : BaseUI() {
    var orderID = ""
    var userID = -1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        rv.layoutManager = LinearLayoutManager(this)
        orderID = intent.getStringExtra("order_id")


        userID = getPrefs().getInt("mId", -1)
        val adapter = ChatAdapter(this)
        rv.adapter = adapter


        Log.i("orderID", orderID)
        helloTV.setOnClickListener {
            adapter.sendMessage("Hello!")
        }
        wruTV.setOnClickListener {
            adapter.sendMessage("On Destination")
        }
        waitTV.setOnClickListener {
            adapter.sendMessage("I m'waiting")
        }
        lateTV.setOnClickListener {
            adapter.sendMessage("On my Way")
        }
        receiveMessage(adapter)

    }

    private fun receiveMessage(adapter: ChatAdapter) {
        val data = BaseUI.fcmData
        if (data != null) {
            val order = Gson().fromJson(data.get("order") as String, Orders::class.java)
            adapter.receiveMessage(data.getString("message"), order.customer_id.toInt())
        }
    }
}
