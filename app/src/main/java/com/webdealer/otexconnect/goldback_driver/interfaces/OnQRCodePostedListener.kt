package com.webdealer.otexconnect.goldback_driver.interfaces

interface OnQRCodePostedListener {
    fun onQRCodeSent()
}