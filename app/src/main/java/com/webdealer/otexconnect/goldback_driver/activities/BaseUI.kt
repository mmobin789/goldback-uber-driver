package com.webdealer.otexconnect.goldback_driver.activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import android.view.Window
import android.widget.ImageView
import android.widget.RelativeLayout
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.vansuita.pickimage.bundle.PickSetup
import com.vansuita.pickimage.dialog.PickImageDialog
import com.vansuita.pickimage.listeners.IPickResult
import com.wang.avi.AVLoadingIndicatorView
import com.webdealer.otexconnect.goldback_driver.helper.PrefManager
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper
import java.io.IOException
import java.util.concurrent.TimeUnit

abstract class BaseUI : AppCompatActivity() {
    fun getUid() = PreferenceManager.getDefaultSharedPreferences(this).getInt("mId", -1)
    lateinit var avatar: String
    lateinit var prefManager: PrefManager
    fun getPrefs(): SharedPreferences {

        return prefManager.pref
    }

    lateinit var progressBar: Dialog
    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    fun fleetMembership(): Boolean {

        return getPrefs().getInt("fleet", -1) != -1

    }

    fun pickImage(baseUI: BaseUI, iPickResult: IPickResult) {
        PickImageDialog.build(PickSetup().setSystemDialog(true), iPickResult).show(baseUI).apply {
            setOnPickCancel { dismiss() }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        CalligraphyConfig.initDefault(CalligraphyConfig.Builder().setFontAttrId(R.attr.fontPath).build())
        progressBar = createProgressBar(this)
        prefManager = PrefManager(this)
        avatar = prefManager.getString("img")
    }

    fun loadWithGlide(path: String, iv: ImageView, circle: Boolean) {
        val request = GlideApp.with(this).load(path).placeholder(R.drawable.logo)
        if (circle)
            request.apply(RequestOptions.circleCropTransform())
        request.into(iv)
    }

    @SuppressLint("MissingPermission")
    protected fun setMapOptions(googleMap: GoogleMap) {
        googleMap.isMyLocationEnabled = true
        googleMap.uiSettings.setAllGesturesEnabled(true)
    }

    protected fun encodeAddress(address: String): LatLng {
        val coder = Geocoder(this)
        var lat = 0.0
        var lng = 0.0
        try {
            val addresses = coder.getFromLocationName(address, 1) as ArrayList<Address>
//            for (add in addresses) {
            val add = addresses[0]
            if (add.hasLatitude() && add.hasLongitude()) {//Controls to ensure it is right address such as country etc.
                lng = add.longitude
                lat = add.latitude
            }


        } catch (e: IOException) {
            e.printStackTrace()
        }
        return LatLng(lat, lng)
    }

    protected fun drawPolyLines(googleMap: GoogleMap, from: LatLng, to: LatLng) {
        // val markerOptions = MarkerOptions()   / code for adding marker to polyline as well
        //     markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.Hue))
        //   markerOptions.position(from)
        //   val markerOptions2 = MarkerOptions()
        //   markerOptions2.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
        //  markerOptions2.position(to)
        //  googleMap.addMarker(markerOptions)
        //  googleMap.addMarker(markerOptions2)
        val line = googleMap.addPolyline(PolylineOptions()
                .add(from, to))
        line.color = Color.BLACK
        line.width = 12f
        line.isGeodesic = true
        val cap = BitmapDescriptorFactory.fromResource(R.drawable.line_cap)
        line.startCap = CustomCap(cap)
        line.endCap = CustomCap(cap)
        line.jointType = JointType.ROUND
        val pattern = listOf(Gap(20f), Dash(20f))
        line.pattern = pattern
//        val builder = LatLngBounds.Builder()
//        builder.include(to)
//        builder.include(from)

        try {
            val bounds = LatLngBounds(from, to)
            googleMap.setLatLngBoundsForCameraTarget(bounds)
            googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 32))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    companion object {
        @JvmStatic
        var fcmData: Bundle? = null

        @JvmStatic
        fun hasLocationPermission(baseUI: BaseUI): Boolean {
            var permission = true
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                permission = ActivityCompat.checkSelfPermission(baseUI, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(baseUI, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                if (!permission) {

                    baseUI.requestPermissions(arrayOf(
                            Manifest
                                    .permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION
                    ), 3)
                }
            }
            return permission
        }

        fun getLocationRequest(seconds: Long): LocationRequest {
            val locationRequest = LocationRequest()
            locationRequest.interval = TimeUnit.SECONDS.toMillis(seconds)
            locationRequest.fastestInterval = TimeUnit.SECONDS.toMillis(10) //10 seconds
            locationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
            locationRequest.smallestDisplacement = 0.1F //1/10 meter
            //   locationRequest.numUpdates = 1

            return locationRequest
        }

        fun buildGoogleApiClient(context: Context, callbacks: GoogleApiClient.ConnectionCallbacks, connectionFailedListener: GoogleApiClient.OnConnectionFailedListener): GoogleApiClient {

            return GoogleApiClient.Builder(context).addConnectionCallbacks(callbacks).addOnConnectionFailedListener(connectionFailedListener).addApi(LocationServices.API).build()


        }


        fun createProgressBar(context: Context): Dialog {

            val dialog = Dialog(context)
            dialog.setCancelable(false)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setOnKeyListener { _, _, p2 ->
                if (p2.keyCode == KeyEvent.KEYCODE_BACK) {
                    dialog.dismiss()

                }
                true
            }
            dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val relativeLayout = RelativeLayout(context)
            val avLoadingIndicatorView = AVLoadingIndicatorView(context)
            avLoadingIndicatorView.setIndicatorColor(ContextCompat.getColor(context, R.color.colorPrimary))
            //avLoadingIndicatorView.setIndicator(BallPulseIndicator())
            val params = RelativeLayout.LayoutParams(150, 150)
            params.addRule(RelativeLayout.CENTER_IN_PARENT)
            avLoadingIndicatorView.layoutParams = params
            relativeLayout.addView(avLoadingIndicatorView)
            dialog.setContentView(relativeLayout)
            return dialog


        }
    }
}