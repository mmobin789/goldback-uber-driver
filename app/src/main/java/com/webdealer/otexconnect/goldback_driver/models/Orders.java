package com.webdealer.otexconnect.goldback_driver.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by fazal on 30/03/2018.
 */

public class Orders {
    public List<Bid> bid;
    public String customer_id, driver_id, fleet_id;
    private String title, description, status, pickup_location, dropoff_location, budget, pickup_time, delivery_time, id;
    @SerializedName("num_of_boxes")
    private String numberOfBoxes;

    public String getNumberOfBoxes() {
        return numberOfBoxes;
    }


    public String getTitle() {
        return title;
    }


    public String getDescription() {
        return description;
    }


    public String getStatus() {
        return status;
    }

    public String getId() {
        return id;
    }

    public String getPickup_location() {
        return pickup_location;
    }


    public String getDropoff_location() {
        return dropoff_location;
    }


    public String getBudget() {
        return budget;
    }


    public String getPickup_time() {
        return pickup_time;
    }


    public String getDelivery_time() {
        return delivery_time;
    }

}
