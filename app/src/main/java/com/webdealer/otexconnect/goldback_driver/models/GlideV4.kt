package com.webdealer.otexconnect.goldback_driver.models

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class GlideV4 : AppGlideModule()