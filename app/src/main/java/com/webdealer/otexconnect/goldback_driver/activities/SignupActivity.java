package com.webdealer.otexconnect.goldback_driver.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hbb20.CountryCodePicker;
import com.webdealer.otexconnect.goldback_driver.R;
import com.webdealer.otexconnect.goldback_driver.models.Signup;
import com.webdealer.otexconnect.goldback_driver.models.User_;
import com.webdealer.otexconnect.goldback_driver.network.RestClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivity extends BaseUI implements View.OnClickListener {


    ImageView mCrossIcon;
    TextView mProceedButton;
    EditText mFirstName;
    EditText mLastName;
    EditText mPhone;
    EditText mEmail;
    EditText mPassword;
    EditText mConfirmPassword;
    Typeface mFont;
    Typeface mFontBold;
    String name;
    String email;
    String password;
    String phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        //getSupportActionBar().hide();
        initView();
    }

    private void initView() {

        mFont = Typeface.createFromAsset(getAssets(), "fonts/Neris-Light.otf");
        mFontBold = Typeface.createFromAsset(getAssets(), "fonts/Neris-SemiBold.otf");

        mProceedButton = findViewById(R.id.proceed);
        mFirstName = findViewById(R.id.fname);
        mLastName = findViewById(R.id.lname);
        mPhone = findViewById(R.id.phone);
        mEmail = findViewById(R.id.email);
        mPassword = findViewById(R.id.password);
        mConfirmPassword = findViewById(R.id.confirm_password);
        mProceedButton.setTypeface(mFontBold);
        mFirstName.setTypeface(mFont);
        mLastName.setTypeface(mFont);
        mPhone.setTypeface(mFont);
        mEmail.setTypeface(mFont);
        mPassword.setTypeface(mFont);
        mConfirmPassword.setTypeface(mFont);
        mCrossIcon = findViewById(R.id.cross);
        mProceedButton.setOnClickListener(this);
        mCrossIcon.setOnClickListener(this);


    }

    private boolean passwordMatch() {

        return mPassword.getText().toString().equals(mConfirmPassword.getText().toString());
    }

    private boolean validate() {

        if (mPassword.getText().toString().isEmpty() || mEmail.getText().toString().isEmpty() || mPhone.getText().toString().isEmpty() || mLastName.getText().toString().isEmpty() || mFirstName.getText().toString().isEmpty())

            return false;

        if (mFirstName.toString().isEmpty()) {

            Toast.makeText(getApplicationContext(), "Please enter first name", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (mLastName.toString().isEmpty()) {

            Toast.makeText(getApplicationContext(), "Please enter last name", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!mPhone.toString().isEmpty() && !Patterns.PHONE.matcher(mPhone.getText()).matches()) {

            Toast.makeText(getApplicationContext(), "Invalid phone number", Toast.LENGTH_SHORT).show();
            return false;
        }


        if (!mEmail.toString().isEmpty() && !Patterns.EMAIL_ADDRESS.matcher(mEmail.getText()).matches()) {

            Toast.makeText(getApplicationContext(), "Invalid email format", Toast.LENGTH_SHORT).show();
            return false;
        }


        if (mPassword.length() < 8) {

            Toast.makeText(getApplicationContext(), "Password must be 8 digits long", Toast.LENGTH_SHORT).show();
            return false;
        }


        email = mEmail.getText().toString();
        phoneNumber = mPhone.getText().toString();
        name = mFirstName.getText().toString() + mLastName.getText().toString();
        password = mPassword.getText().toString();
        return true;
    }


    private void signUp() {

        CountryCodePicker ccp;
        ccp = findViewById(R.id.ccp);

        try {

            RestClient.getServices().signUp(mFirstName.getText().toString(), mEmail.getText().toString(), ccp.getSelectedCountryCodeWithPlus() + mPhone.getText().toString(), mPassword.getText().toString()).enqueue(new Callback<Signup>() {
                @Override
                public void onResponse(Call<Signup> call, Response<Signup> response) {
                    if (response.isSuccessful()) {
                        //----------------------------------------

                        if (response.body().getStatus()) {

                            response.body(); // have your all data
                            User_ user = response.body().getData().getUser();
                            storePrefs(user.getId(), user.getName(), user.getEmail(), user.getPhoneNumber(), user.getBanStatus(), user.getEmailVerificationStatus(), user.getPhoneNumberVerificationStatus());

                            SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
                            prefEditor.putString("mPhone", ccp.getSelectedCountryCodeWithPlus() + mPhone.getText().toString());
                            prefEditor.apply();
                            Intent intent = new Intent(getApplicationContext(), VerificationActivity.class);
                            startActivity(intent);
                        } else {


                            Toast.makeText(SignupActivity.this, "Error: " + response.body().getError().getDescription(), Toast.LENGTH_SHORT).show();
                        }

                        //-------------------------------------------------

                        progressBar.dismiss();

                    }


                }

                @Override
                public void onFailure(Call<Signup> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Please check internet connection", Toast.LENGTH_SHORT).show();
                    progressBar.dismiss();
                }
            });

        } catch (Exception Ex) {

            Toast.makeText(getApplicationContext(), Ex.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    private void storePrefs(int mId, String mName, String mEmail, String mPhone, String ban_status, String email_verification_status, String phone_number_verification_status) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
        prefEditor.clear();
        prefEditor.apply();
        prefEditor.putInt("mId", mId);
        prefEditor.putString("mName", mName);
        prefEditor.putString("mEmail", mEmail);
        prefEditor.putString("mPhone", mPhone);
        prefEditor.putString("payment_id", "0");
        prefEditor.putString("ban_status", ban_status);
        prefEditor.putString("email_verification_status", email_verification_status);
        prefEditor.putString("phone_number_verification_status", phone_number_verification_status);

        //prefEditor.putString("fcm_token", fcm_token);
        prefEditor.apply();

    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.cross) {
            finish();
        }


        if (view.getId() == R.id.proceed) {

            if (validate()) {

                if (passwordMatch()) {

                    progressBar.show();
                    signUp();
//                        Toast.makeText(this, "Proceed", Toast.LENGTH_SHORT).show();
//                        Intent intent=new Intent(this,VerificationActivity.class);
//                        startActivity(intent);
                } else {
                    Toast.makeText(this, "Password doesn't matched", Toast.LENGTH_SHORT).show();
                }

            } else {
            }

        }


    }
}
