package com.webdealer.otexconnect.goldback_driver.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by fazal on 3/13/18.
 */

public class Login {


    @SerializedName("status")

    private Boolean status;

    @SerializedName("data")
    private Data data;

    @SerializedName("error")
    private APIError error;

    public Login(Boolean status) {
        this.status = status;
    }

    public APIError getError() {
        return error;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
