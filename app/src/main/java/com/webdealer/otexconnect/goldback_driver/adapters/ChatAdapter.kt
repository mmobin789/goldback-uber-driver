package com.webdealer.otexconnect.goldback_driver.adapters

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import com.webdealer.otexconnect.goldback_driver.activities.ChatActivity
import com.webdealer.otexconnect.goldback_driver.interfaces.OnMessageListener
import com.webdealer.otexconnect.goldback_driver.models.ChatMessage
import com.webdealer.otexconnect.goldback_driver.network.RestClient

class ChatAdapter(private val chatActivity: ChatActivity) : RecyclerView.Adapter<ViewHolder>() {
    private val pref = chatActivity.prefManager
    private val list: MutableList<ChatMessage>
    private val orderID = chatActivity.orderID
    private val userID = chatActivity.userID
    private val progressBar = chatActivity.progressBar


    init {
        list = pref.loadChat(orderID)
    }

    private val user = 0
    private val customer = 1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layout = if (viewType == user) {
            R.layout.sender
        } else {
            R.layout.receiver
        }
        return ViewHolder(LayoutInflater.from(parent.context).inflate(layout, parent, false))
    }

    fun receiveMessage(message: String, senderID: Int) {
        val chatMessage = ChatMessage(senderID, message, "N/A")
        list.add(chatMessage)
        notifyItemInserted(list.size)
        pref.saveChat(orderID, list)
    }

    fun sendMessage(message: String) {
        progressBar.show()
        val chatMessage = ChatMessage(userID, message, chatActivity.avatar)
        RestClient.sendMessage(orderID.toInt(), message, object : OnMessageListener {
            override fun onMessageSent() {
                progressBar.dismiss()
                Log.i("Chat", "MessageSent")
                list.add(chatMessage)
                notifyItemInserted(list.size)
                pref.saveChat(orderID, list)


            }
        })

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun getItemViewType(position: Int): Int {
        val chatMessage = list[position]
        return if (chatMessage.uid == userID)
            user
        else customer
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(chatActivity, list[position], userID)


    }
}