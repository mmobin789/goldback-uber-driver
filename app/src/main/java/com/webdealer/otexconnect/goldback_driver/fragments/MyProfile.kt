package com.webdealer.otexconnect.goldback_driver.fragments

import android.content.Intent
import android.os.Bundle
import android.telephony.PhoneNumberFormattingTextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.vansuita.pickimage.listeners.IPickResult
import com.webdealer.otexconnect.goldback_driver.activities.FleetChat
import com.webdealer.otexconnect.goldback_driver.helper.PrefManager
import com.webdealer.otexconnect.goldback_driver.interfaces.OnMessageListener
import com.webdealer.otexconnect.goldback_driver.models.APIResponse
import com.webdealer.otexconnect.goldback_driver.network.RestClient
import kotlinx.android.synthetic.main.fragment_profile.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File


class MyProfile : BaseFragment() {
    override fun onFragmentBackPressed() {

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    private fun disableProfileEdit() {
        save.visibility = View.GONE
        addV.visibility = View.INVISIBLE
        addL.visibility = View.INVISIBLE
        addI.visibility = View.INVISIBLE
        upload.visibility = View.GONE
        deleteVR.visibility = View.INVISIBLE
        deleteL.visibility = View.INVISIBLE
        deleteI.visibility = View.INVISIBLE
        etName.isEnabled = false
        etPhone.isEnabled = false
        etEmail.isEnabled = false


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val prefManager = PrefManager(context)
        etName.setText(prefManager.getString("name"))
        etEmail.setText(prefManager.getString("email"))
        etPhone.setText(prefManager.getString("mobile"))
        val vehicle = prefManager.getString("vehicle")
        val license = prefManager.getString("license")
        val insurance = prefManager.getString("insurance")
        val user = prefManager.getString("img")
        loadWithGlide(vehicle, vIv, true)
        loadWithGlide(license, licIV, true)
        loadWithGlide(insurance, iIv, true)
        loadWithGlide(user, img, true)
        etPhone.addTextChangedListener(PhoneNumberFormattingTextWatcher())
        plateTV.text = vehicle
        licTV.text = license
        insuranceTV.text = insurance

        upload.setOnClickListener {
            pickImage(IPickResult {
                if (it.error != null) {
                    Toast.makeText(context, it.error.message, Toast.LENGTH_SHORT).show()
                } else {
                    loadWithGlide(it.path, img, true)
                    prefManager.saveString("img", it.path)
                    progressBar.show()
                    RestClient.uploadDriverImage(progressBar, uid.toString(), File(it.path))
                }
            })

        }

        addV.setOnClickListener {


            pickImage(IPickResult {
                if (it.error != null) {
                    Toast.makeText(context, it.error.message, Toast.LENGTH_SHORT).show()
                } else {
                    loadWithGlide(it.path, vIv, true)
                    plateTV.text = File(it.path).name
                    prefManager.saveString("vehicle", it.path)
                    postVehicleImage(File(it.path))

                }
            })

        }
        addI.setOnClickListener {
            pickImage(IPickResult {
                if (it.error != null) {
                    Toast.makeText(context, it.error.message, Toast.LENGTH_SHORT).show()
                } else {
                    loadWithGlide(it.path, iIv, true)
                    insuranceTV.text = File(it.path).name
                    prefManager.saveString("insurance", it.path)
                    postInsuranceImage(File(it.path))

                }
            })
        }
        addL.setOnClickListener {
            pickImage(IPickResult {
                if (it.error != null) {
                    Toast.makeText(context, it.error.message, Toast.LENGTH_SHORT).show()
                } else {
                    loadWithGlide(it.path, licIV, true)
                    licTV.text = File(it.path).name
                    prefManager.saveString("license", it.path)
                    postLicenseImage(File(it.path))
                }
            })


        }

        deleteL.setOnClickListener {

            //  licenseFile.delete()
            prefManager.removeString("license")

            licIV.setImageResource(R.drawable.logo)

        }
        deleteVR.setOnClickListener {
            //  vehicleFile.delete()
            prefManager.removeString("vehicle")

            vIv.setImageResource(R.drawable.logo)
        }
        deleteI.setOnClickListener {
            //  insuranceFile.delete()
            prefManager.removeString("insurance")
            iIv.setImageResource(R.drawable.logo)
        }

        contact.setOnClickListener {
            startActivity(Intent(it.context, FleetChat::class.java))
        }
        if (fleetMembership()) {
            disableProfileEdit()
            contact.visibility = View.VISIBLE
        }
    }

    private fun postInsuranceImage(file: File) {
        progressBar.show()
        RestClient.postInsuranceImage(context, file, uid, object : OnMessageListener {
            override fun onMessageSent() {
                Toast.makeText(context, "Image Uploaded Successfully", Toast.LENGTH_SHORT).show()
                progressBar.dismiss()
            }
        })
    }

    private fun postVehicleImage(file: File) {
        progressBar.show()
//        val compressed = Utils.getCompressedFile(context!!, file)
//        Log.d("compressedImage", compressed!!.path)
//        val mFile = RequestBody.create(MediaType.parse("image/png"), compressed)
//        val fileToUpload = MultipartBody.Part.createFormData("vehicleURL", compressed.name, mFile)
        RestClient.getServices().postVehicleImage(RestClient.getImageRequestBody(context, file, "vehicleURL"), uid).enqueue(object : Callback<APIResponse> {
            override fun onResponse(call: Call<APIResponse>, response: Response<APIResponse>) {
                if (response.body()!!.status) {


                    Toast.makeText(context, "Image Uploaded Successfully", Toast.LENGTH_SHORT).show()
                } else {

                    Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show()
                }
                progressBar.dismiss()
            }

            override fun onFailure(call: Call<APIResponse>, t: Throwable) {
                Toast.makeText(context, "No internet connection", Toast.LENGTH_SHORT).show()
                progressBar.dismiss()
            }
        })
    }

    private fun postLicenseImage(file: File) {
        //   val compressed = Utils.getCompressedFile(context!!, file)
        progressBar.show()
//        Log.d("compressedImage", compressed!!.path)
//        val mFile = RequestBody.create(MediaType.parse("image/png"), compressed)
//        val fileToUpload = MultipartBody.Part.createFormData("licenseURL", compressed.name, mFile)
        RestClient.getServices().postLicenseImage(RestClient.getImageRequestBody(context, file, "licenseURL"), uid).enqueue(object : Callback<APIResponse> {
            override fun onResponse(call: Call<APIResponse>, response: Response<APIResponse>) {
                if (response.body()!!.status) {
                    Toast.makeText(context, "Image Uploaded Successfully", Toast.LENGTH_SHORT).show()

                } else {

                    Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show()
                }
                progressBar.dismiss()
            }

            override fun onFailure(call: Call<APIResponse>, t: Throwable) {
                progressBar.dismiss()
                Toast.makeText(context, "No internet connection", Toast.LENGTH_SHORT).show()
            }
        })
    }

    companion object {
        @JvmStatic
        fun newInstance() = MyProfile()
    }
}