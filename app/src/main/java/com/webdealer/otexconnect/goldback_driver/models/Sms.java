package com.webdealer.otexconnect.goldback_driver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Sms {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("fourDigitCode")
    @Expose
    private String fourDigitCode;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getFourDigitCode() {
        return fourDigitCode;
    }

    public void setFourDigitCode(String fourDigitCode) {
        this.fourDigitCode = fourDigitCode;
    }

}