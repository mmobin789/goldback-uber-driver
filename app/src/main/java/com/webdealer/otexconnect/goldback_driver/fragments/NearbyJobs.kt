package com.webdealer.otexconnect.goldback_driver.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.webdealer.otexconnect.goldback_driver.adapters.OrderAdapter
import com.webdealer.otexconnect.goldback_driver.models.APIResponse
import com.webdealer.otexconnect.goldback_driver.network.RestClient
import kotlinx.android.synthetic.main.fragment_jobs.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NearbyJobs : BaseFragment() {

    override fun onFragmentBackPressed() {

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_jobs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        recycler_view.layoutManager = LinearLayoutManager(context)
        checkFleetMembership()

    }

    private fun checkFleetMembership() {
        if (fleetMembership()) {
            recycler_view.visibility = View.GONE
            error.text = "Please contact your fleet owner for jobs."
        } else {
            progressBar.show()
            error.visibility = View.GONE
            getAllJobs()
        }
    }

    private fun getAllJobs() {
        RestClient.getServices().jobs.enqueue(object : Callback<APIResponse> {
            override fun onResponse(call: Call<APIResponse>, response: Response<APIResponse>) {
                if (response.body()!!.status) {
                    val mlist = response.body()!!.data.orders




                    recycler_view.adapter = OrderAdapter(mlist)

                    progressBar.dismiss()
                }
            }

            override fun onFailure(call: Call<APIResponse>, t: Throwable) {
                Log.e("error", t.message)
                progressBar.dismiss()

            }
        })
    }

    companion object {
        @JvmStatic
        fun newInstance() = NearbyJobs()
    }
}