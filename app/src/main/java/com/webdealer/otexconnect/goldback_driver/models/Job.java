package com.webdealer.otexconnect.goldback_driver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by fazal on 28/03/2018.
 */

public class Job {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("customer_id")
    @Expose
    private String customer_id;
    @SerializedName("description")
    private String description;
    @SerializedName("budget")
    private String budget;
    @SerializedName("pickup_location")
    private String pickup_location;
    @SerializedName("pickup_time")
    private String pickup_time;
    @SerializedName("dropoff_location")
    private String dropoff_location;
    @SerializedName("delivery_time")
    private String delivery_time;
    @SerializedName("num_of_boxes")
    private String num_of_boxes;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getPickup_location() {
        return pickup_location;
    }

    public void setPickup_location(String pickup_location) {
        this.pickup_location = pickup_location;
    }

    public String getPickup_time() {
        return pickup_time;
    }

    public void setPickup_time(String pickup_time) {
        this.pickup_time = pickup_time;
    }

    public String getDropoff_location() {
        return dropoff_location;
    }

    public void setDropoff_location(String dropoff_location) {
        this.dropoff_location = dropoff_location;
    }

    public String getDelivery_time() {
        return delivery_time;
    }

    public void setDelivery_time(String delivery_time) {
        this.delivery_time = delivery_time;
    }

    public String getNum_of_boxes() {
        return num_of_boxes;
    }

    public void setNum_of_boxes(String num_of_boxes) {
        this.num_of_boxes = num_of_boxes;
    }
}
