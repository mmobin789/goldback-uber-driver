package com.webdealer.otexconnect.goldback_driver.activities;


/**
 * Created by fazal on 18/03/2018.
 */

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.webdealer.otexconnect.goldback_driver.R;
import com.webdealer.otexconnect.goldback_driver.helper.DateValidator;
import com.webdealer.otexconnect.goldback_driver.helper.PrefManager;
import com.webdealer.otexconnect.goldback_driver.models.AddPayment;
import com.webdealer.otexconnect.goldback_driver.network.RestClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PaymentActivity extends BaseUI implements View.OnClickListener {
    EditText mCardHolderName;
    EditText mCardNumber;
    EditText mCardExpiry_mm;
    EditText mCardExpiry_yy;
    EditText mCardCvv;
    Button mSubmitBtn;
    ProgressDialog mProgressDialog;
    private DrawerLayout mDrawerLayout;
    private NavigationView navigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_payment_method);
        initView();


        //-----------------

        mDrawerLayout = findViewById(R.id.drawer_layout);
        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(false);
        actionbar.setTitle("");

        actionbar.setHomeAsUpIndicator(R.drawable.drmenu);

        navigation = findViewById(R.id.nav_view);

        View hView = navigation.getHeaderView(0);
        TextView mNav_title = hView.findViewById(R.id.nav_title);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String mEmail = prefs.getString("mEmail", "email");
        mNav_title.setText(mEmail);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        navigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {


            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                int id = item.getItemId();
                switch (id) {
                    case R.id.nav_logout:

                        Toast.makeText(getApplicationContext(), "Logging out....", Toast.LENGTH_SHORT).show();

                        SharedPreferences sharedPreferences = getSharedPreferences("MyPref", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.clear().apply();
                        PrefManager pref = new PrefManager(getApplicationContext());
                        pref.clearSession();
                        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                        startActivity(i);
                        finish();

                        break;
                    default:
                        Toast.makeText(getApplicationContext(), "Under Development", Toast.LENGTH_SHORT).show();
                        break;


                }

                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);


        //-------------------
    }

    private void initView() {

        mCardHolderName = findViewById(R.id.card_holder_name);
        mCardNumber = findViewById(R.id.card_number);
        mCardExpiry_mm = findViewById(R.id.card_expire_mm);
        mCardExpiry_yy = findViewById(R.id.card_expire_yy);
        mCardCvv = findViewById(R.id.card_cvv);
        mSubmitBtn = findViewById(R.id.submit_btn);
        mSubmitBtn.setOnClickListener(this);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Processing Request...");


    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.submit_btn) {
            if (validate()) {
                mProgressDialog.show();
                AddPayment();

            } else {
                Toast.makeText(this, "Please Enter valid card details", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void AddPayment() {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        int mId = prefs.getInt("mId", 0);


        RestClient.getServices().addPayment(mId, mCardHolderName.getText().toString(), mCardNumber.getText().toString(), mCardCvv.getText().toString(), mCardExpiry_mm.getText().toString(), mCardExpiry_yy.getText().toString())
                .enqueue(new Callback<AddPayment>() {
                    @Override
                    public void onResponse(Call<AddPayment> call, Response<AddPayment> response) {
                        if (response.body().getStatus()) {
                            mProgressDialog.dismiss();

                            Toast.makeText(getApplicationContext(), "Payment Method Added", Toast.LENGTH_SHORT).show();
                            SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
                            prefEditor.putString("payment_id", response.body().getPayment_id());
                            prefEditor.apply();
                            finish();

                        } else {
                            mProgressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<AddPayment> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();

                    }
                });

    }


    private boolean validate() {

        if (mCardHolderName.getText().toString().equals("") || mCardNumber.getText().toString().isEmpty() || mCardCvv.getText().toString().equals("") || mCardExpiry_mm.getText().toString().isEmpty() || mCardExpiry_yy.getText().toString().isEmpty()) {
            return false;
        }

        if (!DateValidator.isValid(mCardExpiry_mm.getText().toString(), mCardExpiry_yy.getText().toString())) {

            Toast.makeText(getApplicationContext(), "Invalid expiry date", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }


}
