package com.webdealer.otexconnect.goldback_driver.models;

/**
 * Created by fazal on 10/04/2018.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Driver {
    @SerializedName("earned")
    public List<Earning> earnings;
    private String rating;
    private int rides;
    @SerializedName("earned_amount")
    private String earnedAmount;
    private int id = 0;
    private String name = "";
    private String email = "";
    private String password = "";
    @SerializedName("phone_number")
    private String phoneNumber = "";
    @SerializedName("profile_image")
    private String profileImage = "";
    @SerializedName("fcm_token")
    @Expose
    private String fcmToken = "";
    @SerializedName("licenseURL")
    @Expose
    private String licenseURL = "";
    @SerializedName("vehicleURL")
    private String vehicleURL = "";
    @SerializedName("fleet_id")
    @Expose
    private Object fleetId = "";
    @SerializedName("type")
    @Expose
    private String type = "";
    @SerializedName("license_verification_status")
    @Expose
    private Integer licenseVerificationStatus = 0;
    @SerializedName("vehicle_verification_status")
    @Expose
    private Integer vehicleVerificationStatus = 0;
    @SerializedName("ban_status")
    @Expose
    private String banStatus = "";
    @SerializedName("email_verification_status")
    @Expose
    private String emailVerificationStatus = "";
    @SerializedName("phone_number_verification_status")
    @Expose
    private String phoneNumberVerificationStatus = "";
    @SerializedName("created_at")
    @Expose
    private String createdAt = "";
    @SerializedName("updated_at")
    @Expose
    private String updatedAt = "";
    @SerializedName("vehicle_name")
    @Expose
    private String vehicleName = "";
    @SerializedName("model")
    @Expose
    private Object model = "";
    @SerializedName("colour")
    @Expose
    private Object colour = "";
    @SerializedName("register_number")
    @Expose
    private Object registerNumber = "";

    public String getEarnedAmount() {
        return earnedAmount;
    }

    public int getRides() {
        return rides;
    }

    public String getRating() {
        return rating;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getProfileImage() {
        return profileImage;
    }


    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public Object getLicenseURL() {
        return licenseURL;
    }

    public void setLicenseURL(String licenseURL) {
        this.licenseURL = licenseURL;
    }

    public String getVehicleURL() {
        return vehicleURL;
    }

    public void setVehicleURL(String vehicleURL) {
        this.vehicleURL = vehicleURL;
    }

    public Object getFleetId() {
        return fleetId;
    }

    public void setFleetId(Object fleetId) {
        this.fleetId = fleetId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getLicenseVerificationStatus() {
        return licenseVerificationStatus;
    }

    public void setLicenseVerificationStatus(Integer licenseVerificationStatus) {
        this.licenseVerificationStatus = licenseVerificationStatus;
    }

    public Integer getVehicleVerificationStatus() {
        return vehicleVerificationStatus;
    }

    public void setVehicleVerificationStatus(Integer vehicleVerificationStatus) {
        this.vehicleVerificationStatus = vehicleVerificationStatus;
    }

    public String getBanStatus() {
        return banStatus;
    }

    public void setBanStatus(String banStatus) {
        this.banStatus = banStatus;
    }

    public String getEmailVerificationStatus() {
        return emailVerificationStatus;
    }

    public void setEmailVerificationStatus(String emailVerificationStatus) {
        this.emailVerificationStatus = emailVerificationStatus;
    }

    public String getPhoneNumberVerificationStatus() {
        return phoneNumberVerificationStatus;
    }

    public void setPhoneNumberVerificationStatus(String phoneNumberVerificationStatus) {
        this.phoneNumberVerificationStatus = phoneNumberVerificationStatus;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


    public String getVehicleName() {
        return vehicleName;
    }


    public Object getModel() {
        return model;
    }

    public void setModel(Object model) {
        this.model = model;
    }

    public Object getColour() {
        return colour;
    }

    public void setColour(Object colour) {
        this.colour = colour;
    }

    public Object getRegisterNumber() {
        return registerNumber;
    }

    public void setRegisterNumber(Object registerNumber) {
        this.registerNumber = registerNumber;
    }


}

