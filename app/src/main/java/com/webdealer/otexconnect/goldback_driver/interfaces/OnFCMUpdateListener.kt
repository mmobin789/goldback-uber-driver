package com.webdealer.otexconnect.goldback_driver.interfaces

interface OnFCMUpdateListener {
    fun onTokenUpdated()
}