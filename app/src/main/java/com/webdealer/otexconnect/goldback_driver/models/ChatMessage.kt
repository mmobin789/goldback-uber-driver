package com.webdealer.otexconnect.goldback_driver.models

import com.webdealer.otexconnect.goldback_driver.utils.Utils

class ChatMessage(val uid: Int, val message: String, val avatar: String) {
    val timestamp = Utils.getCurrentTime()
}