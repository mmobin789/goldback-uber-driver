package com.webdealer.otexconnect.goldback_driver.adapters;

/**
 * Created by fazal on 31/03/2018.
 */

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webdealer.otexconnect.goldback_driver.R;
import com.webdealer.otexconnect.goldback_driver.activities.JobDetailsBids;
import com.webdealer.otexconnect.goldback_driver.activities.JobDetailsInProgress;
import com.webdealer.otexconnect.goldback_driver.models.Orders;

import java.util.List;

import static android.support.v4.content.ContextCompat.startActivity;

public class ScheduledJobsAdapter extends RecyclerView.Adapter<ScheduledJobsAdapter.MyViewHolder> {

    private List<Orders> ordersList;

    public ScheduledJobsAdapter(List<Orders> ordersList) {
        this.ordersList = ordersList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.scheduled_jobs_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Orders job = ordersList.get(position);
        holder.title.setText(job.getTitle());
        holder.description.setText(job.getDescription());


        holder.status.setText(job.getStatus());
        holder.pickup_location.setText(job.getPickup_location());
        holder.dropoff_location.setText(job.getDropoff_location());
        holder.budget.setText(job.getBudget());
        if (job.getStatus().equals("3"))
            holder.jobStatus.setImageResource(R.drawable.completedstatus);
        else if (job.getStatus().equals("2"))
            holder.jobStatus.setImageResource(R.drawable.inprogressstatus);


    }

    @Override
    public int getItemCount() {
        return ordersList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title, description, status, pickup_location, dropoff_location, budget;
        ImageView jobStatus;

        MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.job_row_title);
            description = view.findViewById(R.id.job_row_desc);
            status = view.findViewById(R.id.job_row_status);
            pickup_location = view.findViewById(R.id.job_location_from);
            dropoff_location = view.findViewById(R.id.job_location_to);
            budget = view.findViewById(R.id.job_row_price);
            jobStatus = view.findViewById(R.id.job_status_img);

            itemView.setOnClickListener(this);

        }

        private Class getJobDetail(Orders job) {

            if (job.getStatus().equals("2"))
                return JobDetailsInProgress.class;
            else
                return JobDetailsBids.class;


        }

        @Override
        public void onClick(View view) {

            Orders orders = ordersList.get(getAdapterPosition());
            Intent i = new Intent(view.getContext(), getJobDetail(orders));
            i.putExtra("title", orders.getTitle());
            i.putExtra("id", orders.getId());
            if (orders.bid != null)
                i.putExtra("bid", orders.bid.get(0));
            i.putExtra("description", orders.getDescription());
            i.putExtra("pickup_location", orders.getPickup_location());
            i.putExtra("dropoff_location", orders.getDropoff_location());
            i.putExtra("budget", orders.getBudget());
            startActivity(view.getContext(), i, null);


        }

    }
}