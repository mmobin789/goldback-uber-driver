package com.webdealer.otexconnect.goldback_driver.activities

import android.os.Bundle
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.activity_job_completed.*

// from on done jobs UI
class JobCompleted : BaseUI(), OnMapReadyCallback {
    var p = ""
    var d = ""
    lateinit var from: LatLng
    lateinit var to: LatLng

    override fun onMapReady(p0: GoogleMap) {
        setMapOptions(p0)
        from = encodeAddress(p)
        to = encodeAddress(d)
        drawPolyLines(p0, from, to)
    }

    private fun getData(key: String) = intent.getStringExtra(key)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_job_completed)
        val mapFragment = map as SupportMapFragment
        mapFragment.getMapAsync(this)
        p = getData("pick")
        //  pickUpTV.text = p
        d = getData("drop")
        //  dropOffTV.text = d
        desc.text = getData("desc")
        budget.text = getData("price")
        num.text = getData("num")
        //  ratingBar.rating = getData("rating").toFloat()
    }
}
