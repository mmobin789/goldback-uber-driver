package com.webdealer.otexconnect.goldback_driver.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import com.webdealer.otexconnect.goldback_driver.activities.BaseUI
import com.webdealer.otexconnect.goldback_driver.models.ChatMessage
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.receiver.*
import kotlinx.android.synthetic.main.sender.*

class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
    fun bind(baseUI: BaseUI, chatMessage: ChatMessage, userID: Int) {
        if (chatMessage.uid == userID) // user
        {
            messageSendTV.text = chatMessage.message
            timeSender.text = chatMessage.timestamp
            baseUI.loadWithGlide(chatMessage.avatar, user, true)

        } else // other
        {
            messageReceiveTV.text = chatMessage.message
            timeReceiver.text = chatMessage.timestamp
        }
    }
}