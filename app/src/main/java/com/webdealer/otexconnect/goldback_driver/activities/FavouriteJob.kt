package com.webdealer.otexconnect.goldback_driver.activities

import android.os.Bundle
import android.widget.Toast
import com.google.gson.Gson
import com.webdealer.otexconnect.goldback_driver.interfaces.OnMessageListener
import com.webdealer.otexconnect.goldback_driver.interfaces.OnMyWayListener
import com.webdealer.otexconnect.goldback_driver.models.Orders
import com.webdealer.otexconnect.goldback_driver.network.RestClient
import kotlinx.android.synthetic.main.favoured_job.*

class FavouriteJob : BaseUI() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.favoured_job)
        loadNewJob()
    }

    override fun onBackPressed() {

    }

    private fun loadNewJob() {
        val data = fcmData!!.getString("order")
        val orderID =
                if (data != null) {


                    val orders = Gson().fromJson<Orders>(data, Orders::class.java)



                    job_row_title.text = orders.title
                    desc.text = orders.description
                    budget.text = orders.budget
                    orders.id


                } else {
                    desc.text = intent.getStringExtra("desc")

                    intent.getStringExtra("customer_id")
                    job_row_title.text = intent.getStringExtra("title")
                    budget.text = intent.getStringExtra("price")
                    intent.getStringExtra("order_id")
                }

        accept.setOnClickListener { v ->
            progressBar.show()
            RestClient.acceptJob(prefManager.pref.getInt("mId", -1), orderID, object : OnMyWayListener {
                override fun onMyWay() {
                    progressBar.dismiss()
                    Toast.makeText(v.context, "Accepted", Toast.LENGTH_SHORT).show()
                    fcmData = null
                    finish()
                }

            })
        }
        reject.setOnClickListener { v ->
            progressBar.show()
            RestClient.rejectJob(orderID, object : OnMessageListener {
                override fun onMessageSent() {
                    progressBar.dismiss()
                    Toast.makeText(v.context, "Rejected", Toast.LENGTH_SHORT).show()
                    fcmData = null
                    finish()
                }
            })
        }
    }
}
