package com.webdealer.otexconnect.goldback_driver.interfaces

interface OnBidListener {
    fun onBidUpdated()
}