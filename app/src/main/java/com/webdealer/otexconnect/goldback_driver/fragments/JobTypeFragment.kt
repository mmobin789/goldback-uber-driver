package com.webdealer.otexconnect.goldback_driver.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.webdealer.otexconnect.goldback_driver.adapters.CompletedJobsAdapter
import com.webdealer.otexconnect.goldback_driver.adapters.ScheduledJobsAdapter
import com.webdealer.otexconnect.goldback_driver.interfaces.OnCompletedJobsListener
import com.webdealer.otexconnect.goldback_driver.models.APIResponse
import com.webdealer.otexconnect.goldback_driver.models.Orders
import com.webdealer.otexconnect.goldback_driver.network.RestClient
import kotlinx.android.synthetic.main.fragment_job_type.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class JobTypeFragment : BaseFragment() {
    override fun onFragmentBackPressed() {

    }

    val completedOrders: MutableList<Orders> = mutableListOf()
    val pendingOrders: MutableList<Orders> = mutableListOf()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_job_type, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        recycler_view.layoutManager = LinearLayoutManager(context)
        progressBar.show()
        getScheduledJobs()

    }

    //    companion object {
//        @JvmStatic
//        fun newInstance(): JobTypeFragment = JobTypeFragment()
//    }
    fun showScheduled() {
        recycler_view.adapter = ScheduledJobsAdapter(pendingOrders)
    }

    fun showCompleted() {
        recycler_view.adapter = CompletedJobsAdapter(completedOrders)

    }

    private fun getDoneJobs() {
        RestClient.getDoneJobs(uid, object : OnCompletedJobsListener {
            override fun onJobsCompleted(orders: List<Orders>) {

                completedOrders.addAll(orders)
                progressBar.dismiss()
            }

            override fun onFailed() {
                progressBar.dismiss()
            }
        })

    }

    private fun getScheduledJobs() {

        RestClient.getServices().getScheduledJobs(getPrefs().getInt("mId", -1)).enqueue(
                object : Callback<APIResponse> {
                    override fun onResponse(call: Call<APIResponse>, response: Response<APIResponse>) {
                        if (response.isSuccessful && response.body()!!.status) {
                            val mlist = response.body()!!.data.orders



                            pendingOrders.addAll(mlist)
                            showScheduled()
                            getDoneJobs()


                        }
                    }

                    override fun onFailure(call: Call<APIResponse>, t: Throwable) {
                        Log.e("error", t.message)


                    }
                })
    }
}