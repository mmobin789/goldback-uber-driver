package com.webdealer.otexconnect.goldback_driver.models

import com.google.gson.annotations.SerializedName

data class Earning(val description: String, @field:SerializedName("earned_amount") val earning: String?)