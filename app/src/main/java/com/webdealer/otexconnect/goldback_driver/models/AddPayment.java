package com.webdealer.otexconnect.goldback_driver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by fazal on 27/03/2018.
 */

public class AddPayment {

    @SerializedName("status")
    @Expose
    private Boolean status;

    @SerializedName("message")
    private String message;
    @SerializedName("payment_id")
    private String payment_id;

    public String getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(String payment_id) {
        this.payment_id = payment_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
