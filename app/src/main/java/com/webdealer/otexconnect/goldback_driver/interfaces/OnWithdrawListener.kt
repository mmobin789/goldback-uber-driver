package com.webdealer.otexconnect.goldback_driver.interfaces

interface OnWithdrawListener {
    fun onWithdrawSuccess(message: String)
}