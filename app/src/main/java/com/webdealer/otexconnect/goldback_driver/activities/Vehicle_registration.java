package com.webdealer.otexconnect.goldback_driver.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.enums.EPickType;
import com.webdealer.otexconnect.goldback_driver.R;
import com.webdealer.otexconnect.goldback_driver.helper.PrefManager;
import com.webdealer.otexconnect.goldback_driver.models.APIResponse;
import com.webdealer.otexconnect.goldback_driver.network.RestClient;
import com.webdealer.otexconnect.goldback_driver.utils.Utils;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by fazal on 03/04/2018.
 */

public class Vehicle_registration extends BaseUI implements View.OnClickListener {
    Button mTakePicture;
    Button mBrowseGallery;
    ImageView mPreview;
    ProgressDialog mProgressDialog;
    PickImageDialog pickImageDialog;
    private DrawerLayout mDrawerLayout;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.vehicle_registration);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Processing...");
        mTakePicture = findViewById(R.id.take_photo);
        mBrowseGallery = findViewById(R.id.browser_gallery);
        mPreview = findViewById(R.id.imageView);
        mTakePicture.setOnClickListener(this);
        mBrowseGallery.setOnClickListener(this);

        //-------------------------------

        mDrawerLayout = findViewById(R.id.drawer_layout);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(false);
        actionbar.setTitle("");
        actionbar.setHomeAsUpIndicator(R.drawable.drmenu);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        NavigationView navigation = findViewById(R.id.nav_view);
        View hView = navigation.getHeaderView(0);
        TextView mNav_title = hView.findViewById(R.id.nav_title);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String mEmail = prefs.getString("mEmail", "email");
        mNav_title.setText(mEmail);

        navigation.setNavigationItemSelectedListener(item -> {

            int id = item.getItemId();
            switch (id) {
                case R.id.nav_logout:

                    Toast.makeText(getApplicationContext(), "Logging out....", Toast.LENGTH_SHORT).show();

                    SharedPreferences sharedPreferences = getSharedPreferences("MyPref", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.clear().apply();
                    PrefManager pref = new PrefManager(getApplicationContext());
                    pref.clearSession();
                    Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(i);
                    finish();
                    break;

                case R.id.nav_nearby_jobs:

                    Intent intentSch = new Intent(getApplicationContext(), JobsActivity.class);
                    startActivity(intentSch);

                    break;

                default:
                    Toast.makeText(getApplicationContext(), "Under Development", Toast.LENGTH_SHORT).show();
                    break;

            }

            mDrawerLayout.closeDrawer(GravityCompat.START);
            return false;
        });


    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.take_photo) {
            takePhoto();
        }
        if (view.getId() == R.id.browser_gallery) {
            browsePhoto();
        }
    }

    private void postVehicleImage(File file) {
        File compressed = Utils.getCompressedFile(this, file);
        mProgressDialog.show();
        Log.d("compressedImage", compressed.getPath());
        RequestBody mFile = RequestBody.create(MediaType.parse("image/png"), compressed);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("vehicleURL", compressed.getName(), mFile);
        RestClient.getServices().postVehicleImage(fileToUpload, getUid()).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {
                if (response.body().getStatus()) {

                    mProgressDialog.dismiss();
                    Toast.makeText(Vehicle_registration.this, "Image Uploaded Successfully", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(Vehicle_registration.this, DrivingLicense.class);
                    startActivity(intent);
                } else {
                    mProgressDialog.dismiss();
                    Toast.makeText(Vehicle_registration.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                mProgressDialog.dismiss();
                Toast.makeText(Vehicle_registration.this, "No internet connection", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void takePhoto() {

        pickImageDialog = PickImageDialog.build(new PickSetup().setPickTypes(EPickType.CAMERA), pickResult -> {
            loadWithGlide(pickResult.getPath(), mPreview, false);
            postVehicleImage(new File(pickResult.getPath()));
        }).show(getSupportFragmentManager()).setOnPickCancel(() -> {
            pickImageDialog.dismiss();
        });

    }

    private void browsePhoto() {
        pickImageDialog = PickImageDialog.build(new PickSetup().setPickTypes(EPickType.GALLERY), pickResult -> {
            loadWithGlide(pickResult.getPath(), mPreview, false);
            postVehicleImage(new File(pickResult.getPath()));
        }).show(getSupportFragmentManager()).setOnPickCancel(() -> {
            pickImageDialog.dismiss();
        });
    }


}
