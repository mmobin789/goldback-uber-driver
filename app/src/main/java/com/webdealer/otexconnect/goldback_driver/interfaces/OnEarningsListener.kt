package com.webdealer.otexconnect.goldback_driver.interfaces

import com.webdealer.otexconnect.goldback_driver.models.Driver

interface OnEarningsListener {
    fun onEarnings(drivers: List<Driver>)
}