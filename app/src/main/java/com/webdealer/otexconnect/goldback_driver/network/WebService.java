package com.webdealer.otexconnect.goldback_driver.network;

import com.webdealer.otexconnect.goldback_driver.models.APIResponse;
import com.webdealer.otexconnect.goldback_driver.models.AddPayment;
import com.webdealer.otexconnect.goldback_driver.models.Login;
import com.webdealer.otexconnect.goldback_driver.models.Signup;
import com.webdealer.otexconnect.goldback_driver.models.Sms;
import com.webdealer.otexconnect.goldback_driver.models.Verify;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by fazal on 3/13/18.
 */

public interface WebService {

    @FormUrlEncoded
    @POST("login-driver")
    Call<Login> login(@Field("email") String email, @Field("password") String password);

    @FormUrlEncoded
    @POST("register-driver")
    Call<Signup> signUp(@Field("name") String name, @Field("email") String email, @Field("phone_number") String phoneNumber, @Field("password") String password);

    @FormUrlEncoded
    @POST("send-sms-code")
    Call<Sms> sendSMS(@Field("phone_number") String phone_number);

    @FormUrlEncoded
    @POST("update-driver-fcm-token")
    Call<APIResponse> updateFCM(@Field("driver_id") String driver_id, @Field("fcm_token") String fcm_token);

    @FormUrlEncoded
    @POST("verify-sms-status")
    Call<Verify> Verify(@Field("id") int id, @Field("type") String type);

    @FormUrlEncoded
    @POST("set-payment-detail")
    Call<AddPayment> addPayment(@Field("driver_id") int id, @Field("cc_name") String cc_name, @Field("cc_number") String cc_number, @Field("cvc_number") String cvc_number, @Field("expiry_month") String expiry_month, @Field("expiry_year") String expiry_year);

    @FormUrlEncoded
    @POST("order-post")
    Call<APIResponse> addJob(@Field("driver_id") int id, @Field("title") String title, @Field("description") String description, @Field("pickup_location") String pickup_location, @Field("pickup_time") String pickup_time, @Field("dropoff_location") String dropoff_location, @Field("delivery_time") String delivery_time, @Field("budget") String budget, @Field("num_of_boxes") String num_of_boxes);

    @GET("get-all-jobs")
    Call<APIResponse> getJobs();

    @FormUrlEncoded
    @POST("bid")
    Call<APIResponse> postBid(@Field("driver_id") int mId, @Field("order_id") String Order_id, @Field("bid") String Bid);

    @FormUrlEncoded
    @POST("buyNow")
    Call<APIResponse> postBuyNow(@Field("driver_id") int mId, @Field("order_id") String Order_id);

    @Multipart
    @POST("vehicle-image")
    Call<APIResponse> postVehicleImage(@Part MultipartBody.Part vehicleURL, @Part("id") Integer id);

    @Multipart
    @POST("license-image")
    Call<APIResponse> postLicenseImage(@Part MultipartBody.Part licenseURL, @Part("id") Integer id);

    @Multipart
    @POST("insurance-image")
    Call<APIResponse> postInsuranceImage(@Part MultipartBody.Part image, @Part("id") int id);

    @FormUrlEncoded
    @POST("driver-complete-orders")
    Call<APIResponse> getDoneJobs(@Field("driver_id") int id);

    @FormUrlEncoded
    @POST("driver-pending-orders")
    Call<APIResponse> getScheduledJobs(@Field("driver_id") int id);

    @FormUrlEncoded
    @POST("driver-earning")
    Call<APIResponse> getDriverEarnings(@Field("driver_id") int id);

    @FormUrlEncoded
    @POST("driverOnWayNotification")
    Call<ResponseBody> onMyWay(@Field("order_id") String id);

    @POST("qrCodes")
    Call<APIResponse> getQrCodes(@Query("order_id") String id);

    @Multipart
    @POST("upload-order-images")
    Call<ResponseBody> postQR(@Part MultipartBody.Part imageURL, @Part("qrCode") RequestBody qrCode, @Part("order_id") RequestBody orderID);

    @POST("update-bid")
    Call<ResponseBody> updateBid(@Query("bid_id") int bidID, @Query("bid") double bid);

    @POST("delete-bid")
    Call<ResponseBody> deleteBid(@Query("bid_id") int bidID);

    @POST("latlong")
    Call<ResponseBody> sendLocation(@Query("order_id") String orderID, @Query("lat") String lat, @Query("long") String lng);

    @POST("sendMessageToCustomer")
    Call<APIResponse> sendMessageToCustomer(@Query("order_id") int orderID, @Query("message") String message);

    @POST("sendMessageToFleet")
    Call<APIResponse> sendMessageToFleet(@Query("driver_id") int id, @Query("message") String message);

    @POST("withdraw-money")
    Call<APIResponse> withdraw(@Query("driver_id") int id);

    @Multipart
    @POST("driver-profile-image")
    Call<APIResponse> uploadDriverImage(@Part MultipartBody.Part imageURL, @Part("driver_id") RequestBody driverID);

    @POST("driverContactus")
    Call<APIResponse> contactUs(@Query("driver_id") int id, @Query("message") String msg);

    @POST("accept-notification")
    Call<APIResponse> acceptJob(@Query("driver_id") int id, @Query("order_id") String orderID);

    @POST("reject-notification")
    Call<APIResponse> rejectJob(@Query("order_id") String orderID);

}
