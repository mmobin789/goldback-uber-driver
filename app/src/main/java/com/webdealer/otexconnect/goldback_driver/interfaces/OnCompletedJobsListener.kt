package com.webdealer.otexconnect.goldback_driver.interfaces

import com.webdealer.otexconnect.goldback_driver.models.Orders


interface OnCompletedJobsListener {
    fun onJobsCompleted(orders: List<Orders>)
    fun onFailed()
}