package com.webdealer.otexconnect.goldback_driver.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.webdealer.otexconnect.goldback_driver.R;
import com.webdealer.otexconnect.goldback_driver.adapters.OrderAdapter;
import com.webdealer.otexconnect.goldback_driver.fragments.BaseFragment;
import com.webdealer.otexconnect.goldback_driver.fragments.ContactUsFragment;
import com.webdealer.otexconnect.goldback_driver.fragments.EarningsFragment;
import com.webdealer.otexconnect.goldback_driver.fragments.MyJobsFragment;
import com.webdealer.otexconnect.goldback_driver.fragments.MyProfile;
import com.webdealer.otexconnect.goldback_driver.fragments.NearbyJobs;
import com.webdealer.otexconnect.goldback_driver.fragments.SettingsFragment;
import com.webdealer.otexconnect.goldback_driver.interfaces.OnFragmentBackPressListener;
import com.webdealer.otexconnect.goldback_driver.models.Orders;
import com.webdealer.otexconnect.goldback_driver.network.RestClient;
import com.webdealer.otexconnect.goldback_driver.network.WebService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fazal on 30/03/2018.
 */

public class JobsActivity extends BaseUI {

    public List<Orders> ordersList = new ArrayList<>();
    public TextView header;
    WebService mWebService;
    private RecyclerView recyclerView;
    private OrderAdapter mAdapter;
    private DrawerLayout mDrawerLayout;
    private NavigationView navigation;
    private BaseFragment fragment;
    private OnFragmentBackPressListener onFragmentBackPressListener;

    public void setOnFragmentBackPressListener(OnFragmentBackPressListener onFragmentBackPressListener) {
        this.onFragmentBackPressListener = onFragmentBackPressListener;
    }


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_jobs);

        //recyclerView = (RecyclerView) findViewById(R.id.recycler_view);


        //-------------------------------------------------------
        mDrawerLayout = findViewById(R.id.drawer_layout);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setTitle("");
        actionbar.setHomeAsUpIndicator(R.drawable.drmenu);

        navigation = findViewById(R.id.nav_view);
        View hView = navigation.getHeaderView(0);
        TextView mNav_title = hView.findViewById(R.id.nav_title);


        String mEmail = prefManager.pref.getString("mEmail", "email");
        mNav_title.setText(mEmail);

        header = (TextView) toolbar.getChildAt(0);
//        findViewById(R.id.notification_btn).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                header.setText("Notifications");
//                fragment = Notifications.newInstance();
//                setFragment();
//            }
//        });
        fragment = NearbyJobs.newInstance();
        setFragment();
        if (fleetMembership()) {
            navigation.getMenu().findItem(R.id.nav_earnings).setVisible(false);
        }

        navigation.setNavigationItemSelectedListener(item -> {

            int id = item.getItemId();

            switch (id) {
                case R.id.nav_logout:

                    Toast.makeText(getApplicationContext(), "Logging out....", Toast.LENGTH_SHORT).show();
                    RestClient.updateFCM(getUid(), "", () -> {
                        prefManager.clearSession();
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(i);
                        finish();
                    });

                    break;

                case R.id.nav_nearby_jobs:
                    header.setText("Nearby Jobs");
                    fragment = NearbyJobs.newInstance();
//                        Intent intentSch = new Intent(getApplicationContext(), JobsActivity.class);
//                        startActivity(intentSch);

                    break;
                case R.id.nav_profile:
                    header.setText("My Profile");
                    fragment = MyProfile.newInstance();
                    break;
                case R.id.nav_my_jobs:
                    header.setText("Scheduled Jobs");
                    fragment = MyJobsFragment.newInstance();
                    break;
                case R.id.nav_support:
                    header.setText("Contact Us");
                    fragment = ContactUsFragment.newInstance();
                    break;
                case R.id.nav_settings:
                    header.setText("Settings");
                    fragment = SettingsFragment.newInstance();
                    break;

                case R.id.nav_earnings:
                    header.setText("Earnings");
                    fragment = EarningsFragment.newInstance();
                    break;
                default:
                    Toast.makeText(getApplicationContext(), "Under Development", Toast.LENGTH_SHORT).show();
                    break;

            }

            setFragment();
            mDrawerLayout.closeDrawer(GravityCompat.START);
            return false;
        });


//        RestClient.getServices().getJobs().enqueue(new Callback<APIResponse>() {
//            @Override
//            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
//                if (response.body().getStatus()) {
//                    List<Orders> mlist = response.body().getData().getOrders();
//
//
//                    recyclerView = findViewById(R.id.recycler_view);
//
//
//                    mAdapter = new OrderAdapter(mlist);
//                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
//                    recyclerView.setLayoutManager(mLayoutManager);
//                    recyclerView.setItemAnimator(new DefaultItemAnimator());
//
//                    recyclerView.setAdapter(mAdapter);
//
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<APIResponse> call, Throwable t) {
//                Log.e("error", t.getMessage());
//
//            }
//        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        checkFavouredJob();
    }

    private String getNotificationData() {
        String value = null;
        if (getFcmData() != null)
            value = getFcmData().getString("order");
        if (value != null) {
            Log.i("FCM Data Order", value);

        }

        return value;
    }

    private void checkFavouredJob() {
        Intent i = new Intent(this, FavouriteJob.class);
        if (getNotificationData() != null) {
            startActivity(i);
        } else if (getIntent().getStringExtra("order_id") != null) {
            i.putExtra("desc", getIntent().getStringExtra("desc"));
            i.putExtra("order_id", getIntent().getStringExtra("order_id"));
            i.putExtra("customer_id", getIntent().getStringExtra("customer_id"));
            i.putExtra("title", getIntent().getStringExtra("title"));
            i.putExtra("price", getIntent().getStringExtra("price"));
            startActivity(i);
            getIntent().removeExtra("order_id");


        }
    }

    @Override
    public void onBackPressed() {
        if (fragment != null) {
            onFragmentBackPressListener.onFragmentBackPressed();
        } else
            super.onBackPressed();
    }

    private void setFragment() {
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}






