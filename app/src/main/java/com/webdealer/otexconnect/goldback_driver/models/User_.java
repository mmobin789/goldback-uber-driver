package com.webdealer.otexconnect.goldback_driver.models;

import com.google.gson.annotations.SerializedName;

public class User_ {
    @SerializedName("fleet_id")
    public int fleetId = -1;
    public String licenseURL, vehicleURL, insuranceURL;
    @SerializedName("profile_image")
    public String profileImage;
    private int id;
    private String name;
    private String email;
    private String password;
    @SerializedName("phone_number")
    private String phoneNumber;
    @SerializedName("payment_id")
    private String payment_id;
    @SerializedName("fcm_token")
    private String fcmToken;
    @SerializedName("ban_status")
    private String banStatus;
    @SerializedName("email_verification_status")
    private String emailVerificationStatus;
    @SerializedName("phone_number_verification_status")
    private String phoneNumberVerificationStatus;
    @SerializedName("license_verification_status")
    private String license_verification_status;
    @SerializedName("vehicle_verification_status")
    private String vehicle_verification_status;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("updated_at")

    private String updatedAt;

    public int getId() {
        return id;
    }


    public String getName() {
        return name;
    }


    public String getEmail() {
        return email;
    }


    public String getPassword() {
        return password;
    }


    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPayment_id() {
        return payment_id;
    }


    public String getFcmToken() {
        return fcmToken;
    }


    public String getBanStatus() {
        return banStatus;
    }


    public String getEmailVerificationStatus() {
        return emailVerificationStatus;
    }


    public String getPhoneNumberVerificationStatus() {
        return phoneNumberVerificationStatus;
    }


    public String getLicense_verification_status() {
        return license_verification_status;
    }


    public String getVehicle_verification_status() {
        return vehicle_verification_status;
    }

    public String getCreatedAt() {
        return createdAt;
    }


    public String getUpdatedAt() {
        return updatedAt;
    }


}
