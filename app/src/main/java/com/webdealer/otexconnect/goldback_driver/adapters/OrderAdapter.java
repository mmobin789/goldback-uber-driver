package com.webdealer.otexconnect.goldback_driver.adapters;

/**
 * Created by fazal on 31/03/2018.
 */

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.webdealer.otexconnect.goldback_driver.R;
import com.webdealer.otexconnect.goldback_driver.models.APIResponse;
import com.webdealer.otexconnect.goldback_driver.models.Orders;
import com.webdealer.otexconnect.goldback_driver.network.RestClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.MyViewHolder> {

    private List<Orders> ordersList;
    private String m_Text = "";


    public OrderAdapter(List<Orders> ordersList) {
        this.ordersList = ordersList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.jobs_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Orders job = ordersList.get(position);

        holder.title.setText(job.getTitle());
        holder.description.setText(job.getDescription());
        //  holder.status.setText(job.getStatus());
        holder.pickup_location.setText(job.getPickup_location());
        holder.dropoff_location.setText(job.getDropoff_location());
        holder.budget.setText(job.getBudget());

        if (job.getPickup_time().equals(job.getDelivery_time())) {
            holder.mBtnBidTitle.setText("Buy Now");
        }


    }

    private void removeItem(int position) {
        ordersList.remove(position);
        notifyItemRemoved(position);

    }

    @Override
    public int getItemCount() {
        return ordersList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        public TextView title, description, pickup_location, dropoff_location, budget, mBtnBidTitle;
        public LinearLayout mBtnBid;


        public MyViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.job_row_title);
            description = itemView.findViewById(R.id.job_row_desc);
            //  status = itemView.findViewById(R.id.job_row_status);
            pickup_location = itemView.findViewById(R.id.job_location_from);
            dropoff_location = itemView.findViewById(R.id.job_location_to);
            budget = itemView.findViewById(R.id.job_row_price);
            mBtnBidTitle = itemView.findViewById(R.id.btnTitle);
            mBtnBid = itemView.findViewById(R.id.bid);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            mBtnBid.setOnClickListener(v -> {
                Orders job = ordersList.get(getAdapterPosition());
                if (mBtnBidTitle.getText().equals("Bid")) {
                    //--------------------------------------------------------------------------
                    AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                    builder.setTitle("Enter bid amount");

// Set up the input
                    final EditText input = new EditText(v.getContext());
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                    input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_CLASS_NUMBER);
                    builder.setView(input);

// Set up the buttons
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            m_Text = input.getText().toString();
                            int BidInput = Integer.parseInt(m_Text);

                            int Budget = Integer.parseInt(budget.getText().toString());
                            //holder.mBtnBid.setVisibility(View.GONE);
                            if (BidInput <= Budget) {
                                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(v.getContext());
                                int mId = prefs.getInt("mId", 0);
                                //removeItem(holder.getAdapterPosition());
                                RestClient.getServices().postBid(mId, job.getId(), m_Text).enqueue(new Callback<APIResponse>() {
                                    @Override
                                    public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                                        if (response.body().getStatus()) {

                                            Toast.makeText(v.getContext(), "Bid Posted", Toast.LENGTH_SHORT).show();
                                            removeItem(getAdapterPosition());
                                        } else {
                                            Toast.makeText(v.getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                                        }

                                    }

                                    @Override
                                    public void onFailure(Call<APIResponse> call, Throwable t) {
                                        Toast.makeText(v.getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                });
                            } else {

                                Toast.makeText(v.getContext(), "Invalid bid amount", Toast.LENGTH_SHORT).show();
                            }


                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    builder.show();
                    //---------------------------------------------------------------------------
                } else {
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(v.getContext());
                    int mId = prefs.getInt("mId", 0);

                    RestClient.getServices().postBuyNow(mId, job.getId()).enqueue(new Callback<APIResponse>() {
                        @Override
                        public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                            Toast.makeText(v.getContext(), "Success", Toast.LENGTH_SHORT).show();
                            removeItem(getAdapterPosition());
                        }

                        @Override
                        public void onFailure(Call<APIResponse> call, Throwable t) {
                            Toast.makeText(v.getContext(), "Network Error", Toast.LENGTH_SHORT).show();
                        }
                    });

                }

            });
        }


        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.bid) {

            }

        }

        @Override
        public boolean onLongClick(View v) {

            if (v.getId() == R.id.bid) {

            }
            return true;
        }
    }
}