package com.webdealer.otexconnect.goldback_driver.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.vansuita.pickimage.listeners.IPickResult
import com.webdealer.otexconnect.goldback_driver.interfaces.OnMessageListener
import com.webdealer.otexconnect.goldback_driver.network.RestClient
import kotlinx.android.synthetic.main.activity_insurance.*
import java.io.File

class Insurance : BaseUI(), OnMessageListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_insurance)
        init()
    }

    private fun init() {
        take_driver_photo.setOnClickListener {
            pickImage(this, IPickResult {
                loadWithGlide(it.path, mPreview, false)
                postInsuranceImage(File(it.path))
            })
        }
        browser_driver_gallery.setOnClickListener {
            pickImage(this, IPickResult {

                loadWithGlide(it.path, mPreview, false)
                postInsuranceImage(File(it.path))
            })
        }
    }

    private fun postInsuranceImage(file: File) {
        progressBar.show()
        RestClient.postInsuranceImage(this, file, getUid(), this)
    }

    override fun onMessageSent() {
        Toast.makeText(this, "Image Successfully Uploaded.", Toast.LENGTH_SHORT).show()
        progressBar.dismiss()
        val intent = Intent(this, JobsActivity::class.java)
        startActivity(intent)
    }
}
