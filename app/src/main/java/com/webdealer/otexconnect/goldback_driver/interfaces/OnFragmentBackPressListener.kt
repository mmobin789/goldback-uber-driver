package com.webdealer.otexconnect.goldback_driver.interfaces

interface OnFragmentBackPressListener {
    fun onFragmentBackPressed()
}