package com.webdealer.otexconnect.goldback_driver.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {

    @SerializedName("driver")
    public List<Driver> drivers;
    @SerializedName("qrcodes")
    public List<String> qrCodes;
    private User_ user;
    private Job job;
    private String order_id;
    private List<Orders> orders;

    public List<Orders> getOrders() {
        return orders;
    }


    public User_ getUser() {
        return user;
    }


    public Job getJob() {
        return job;
    }


    public String getOrder_id() {
        return order_id;
    }


}