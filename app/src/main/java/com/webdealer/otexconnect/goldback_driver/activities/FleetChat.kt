package com.webdealer.otexconnect.goldback_driver.activities

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.webdealer.otexconnect.goldback_driver.adapters.FleetChatAdapter
import kotlinx.android.synthetic.main.activity_chat.*

class FleetChat : BaseUI() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        initUI()

    }

    private fun initUI() {
        menu.visibility = View.GONE
        img.visibility = View.GONE
        val adapter = FleetChatAdapter(this)
        chatInput.visibility = View.VISIBLE
        rv.layoutManager = LinearLayoutManager(this)
        rv.adapter = adapter
        receiveMessage(adapter)
        submit_btn.setOnClickListener {
            val message = etInput.text.toString()
            if (message.isNotBlank())
                adapter.sendMessage(etInput, message)
        }

    }

    private fun receiveMessage(adapter: FleetChatAdapter) {
        val data = BaseUI.fcmData
        if (data != null) {

            adapter.receiveMessage(data.getString("message"), -1)
        }
    }
}
